\documentclass[a4paper,12pt]{article}

% Paquetes principales
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[spanish]{babel}
\usepackage{graphicx}
\usepackage[margin=2.5cm]{geometry}

% Paquete para los hiperenlaces
\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=black,     
    urlcolor=blue,
}

% Paquete para el encabezado
\usepackage{fancyhdr}
\setlength{\headheight}{15pt}
\pagestyle{fancy}
\lhead{Web de catalogación y biblioteca de literatura}
\rhead{Ignacio Torre Suárez}

% Flotación de imágenes
\usepackage{float}

% Espaciado
\usepackage{setspace}
\setstretch{1.5}
\setlength\parskip{6pt}
\setlength\parindent{0pt}

% Código y órdenes
\usepackage{listings}

% Acrónimos
\usepackage[acronym,nomain]{glossaries}
% Generate the glossary
\makeglossaries

% Portada personalizada
\title{Web de catalogación y biblioteca de literatura}
\date{5 de junio de 2021}
\author{Ignacio Torre Suárez}

\makeatletter
\renewcommand{\maketitle}{
\begin{center}

	\pagestyle{empty}
	\phantom{.}  %necessary to add space on top before the title
	\vspace{3cm}

	{\Huge \bf \@title\par}
	\vspace{2.5cm}

	{\LARGE \@author}\\[1cm]

	{\Large\@date}

	\vspace{3cm}

	\Large Tutor individual: \textbf{José Carlos Zurita Ciruela}\\
	Tutora colectiva: \textbf{María Concepción Peinó Alonso}
	\vfill
	{CIFP de Avilés}\hspace{1cm}{ 2º Desarrollo de Aplicaciones Web}\\[2cm]
	%if you want something in the bottom of the page just use \vfill before that.

\end{center}
}\makeatother

% Inicio del documento
\begin{document}
	\pagenumbering{gobble}
	\maketitle
	\newpage
	\pagenumbering{arabic}

\part{Desarrollo del proyecto}
\section{Índice}
\tableofcontents
\newpage
\section{Índice de figuras}
\listoffigures
\newpage
\section{Siglas y acrónimos}
\newacronym{mvc}{MVC}{Modelo-Vista-Controlador}
\newacronym{cpu}{CPU}{Central Processing Unit}
\newacronym{gpu}{GPU}{Graphics Processing Unit}
\newacronym{ssd}{SSD}{Solid-State Drive}
\newacronym{hdd}{HDD}{Hard Disk Drive}
\newacronym{xml}{XML}{Extensible Markup Language}
\newacronym{json}{JSON}{JavaScript Object Notation}
\newacronym{api}{API}{Application Programming Interface}
\newacronym{rest}{REST}{REpresentational State Transfer}
\newacronym{jar}{JAR}{Java Archive}
\printglossaries

\section{Introducción}
Este documento es la memoria de la aplicación Web de catalogación y biblioteca de literatura (\textbf{webiblio}), proyecto de fin del ciclo DAW, de \textbf{Ignacio Torre Suárez}.

En esta memoria se detallarán los distintos\textbf{ procesos del desarrollo del proyecto}: análisis, diseño, desarrollo, pruebas y despliegue. También se detallarán los requisitos y presupuesto del proyecto, además del software y hardware utilizado.

La memoria también contendrá una \textbf{guía de instalación} para la puesta en producción del proyecto y un \textbf{manual de usuario}.

\section{Descripción del proyecto}

\subsection{Objetivos del proyecto}
En la web existen  páginas llamadas ”\textbf{comunidades virtuales de catalogación}”, las cuales permiten a sus usuarios registrarse con el objetivo de \textbf{crear y compartir listas de elementos} los cuales son de su interés: videojuegos, series, libros, películas… Estas páginas permiten, por ejemplo, crear listas de libros favoritos, libros que uno tiene pendiente de leer, libros ya leídos o libros que no se han terminado.  Ejemplos de este tipo de páginas serían \textit{goodreads}, dedicada a libros y \textit{FilmAffinity}, dedicada a películas.

Por otra parte, en lo que a libros respecta, existen webs llamadas “\textbf{bibliotecas electrónicas}” cuyo objetivo es \textbf{archivar y ofrecer libros en formato digital}, que por lo general ya han pasado a dominio público. Una de las más populares es la web \textit{Project Gutenberg}.
Mientras que las \textbf{comunidades virtuales} de catalogación ofrecen una extensa variedad de información y \textbf{herramientas} para crear listas, estas no ofrecen ni facilitan el contenido en sí (en este caso, los libros digitales)

En contraposición, las \textbf{bibliotecas digitales} ofrecen una gran variedad de \textbf{contenido} en forma de libros digitales, pero muchas se sienten desfasadas y ofrecen pocas herramientas para categorizar o filtrar su contenido.

La idea general del proyecto consiste en \textbf{la unión de lo mejor de los dos conceptos} mencionados anteriormente en lo que a temática de literatura respecta: las funciones para listar, organizar y filtrar elementos de una comunidad virtual de catalogación más los servicios de archivo y preservación de una biblioteca digital.

Se ha optado por centrarse en una \textbf{temática de libros y literatura} por la facilidad de obtener obras de este tipo de \textbf{forma legal} ya que muchas de ellas ya se encuentran bajo el dominio público, a diferencia de otros medios más novedosos como las películas o los videojuegos los cuales en su gran mayoría todavía se encuentran bajo copyright o derechos de autor.

El objetivo general del proyecto es la \textbf{creación de una página web}  la cual ofrezca a sus usuarios servicios de una \textbf{comunidad virtual de catalogación} enfocada a \textbf{literatura y libros}, además de los servicios de una\textbf{ biblioteca digital} para archivar y ofrecer esos libros.

Esta página permitirá el \textbf{registro de usuarios}, los cuales podrán crear y gestionar sus \textbf{listas personales}, puntuar sus libros y ver, buscar y descargar libros en la biblioteca digital. También podrán sugerir la adición de nuevos libros a la web.

Además de este tipo de usuarios, también existirán \textbf{usuarios admistradores} con opciones de administración respecto al resto de usuarios y libros de la página, y usuarios \textbf{moderadores} que podrán aprobar la subida de nuevos libros y deshabilitar las cuentas de ciertos usuarios.

\subsection{Requisitos mínimos y restricciones}

\paragraph{Cambios en las tecnologías utilizadas:}

En el \textbf{anteproyecto} se estableción que la parte del servidor de la aplicación web sería realizada mediante el \textbf{framework de PHP Laravel}. Sin embargo, se ha decidido cambiar al \textbf{framework de Java Spring}.

Laravel y Spring \textbf{cumplen la misma función}: son \textbf{frameworks enfocados al desarrollo web} que se basan en la arquitectura \gls{mvc}. Aun así, se ha decidido utilizar \textbf{Java y Spring Boot} en vez de PHP y Laravel, ya que se ha observado que el primero es bastante más usado y más popular en las empresas del sector regional.

\paragraph{Requisitos mínimos y restricciones:}

La página constará de \textbf{cuatro roles de usuario}: usuarios anónimos (no registrados), usuarios rasos, moderadores y administradores.

A continuación se indicarán las acciones de la página web junto al rol o roles necesarios para poder acceder a esas acciones:
\begin{itemize}
	\item \textbf{Registro de usuario} (usuarios anónimos): si un usuario no ha iniciado sesión, tendrá la opción de registrarse y crear una cuenta de usuario. Las cuentas de usuario creadas mediante el registro de la web tendrán el rol de usuario raso.
	\item \textbf{Inicio de sesión} (usuarios anónimos): si un usuario no ha iniciado sesión, tendrá la opción de autenticarse e iniciar sesión con su cuenta y rol.
	\item \textbf{Ver y buscar libros} (todos los roles): la aplicación permitirá ver una lista de los libros que tiene en su base de datos, además de opciones de búsqueda y filtrado respecto a características de esos libros.
	\item \textbf{Ver libro} (todos los roles): al acceder a un libro en concreto, se podrá encontrar información más detallada sobre este. También podrá ser descargado o ser visto en el propio navegador (si el formato del libro y el navegador lo permiten)
	\item \textbf{Subir libro} (usuarios rasos/moderadores/administradores): esta opción permitirá subir un libro a la propia web. Su funcionamiento dependerá del rol del usuario:
		\begin{itemize}
			\item \textbf{Usuarios rasos}: podrán subir libros, pero estos deberán ser confirmados por un moderador o administrador.
			\item \textbf{Moderadores y administradores}: podrán subir libros sin la necesidad de confirmación por parte de otro usuario o rol.
		\end{itemize}
	\item \textbf{Perfil de usuario y listas} (usuarios rasos/moderadores/administradores): estos usuarios tendrán un perfil, donde podrán ver, editar y categorizar sus propias listas creadas con libros de la base de datos de la página. Dependiendo del nivel de privacidad del perfil, estas listas podrán ser vistas por otros usuarios.
	\item \textbf{Deshabilitar usuario} (moderadores/administradores): un usuario podrá ser deshabilitado, impidiéndole el acceso al inicio de sesión. Los moderadores solo podrán deshabilitar usuarios rasos, mientras que los administradores podrán deshabilitar todo tipo de usuarios.
	\item \textbf{Confirmar/denegar subida de libro} (moderadores/administradores): los administradores y moderadores podrán confirmar o denegar la subida de un libro por parte de un usuario raso.
	\item \textbf{Crear usuarios, borrar usuarios, borrar libros} (administrador): un administrador tendrá acceso a la creación y borrado de todo tipo de usuarios, además de acceso a la eliminación directa de libros de la base de datos.
\end{itemize}

Todas estas acciones estarán distribuidas en \textbf{ múltiples páginas de la web} las cuales serán accesibles dependiendo del rol del usuario.

\section{Métodos y herramientas de desarrollo}
\subsection{Hardware}
Para el desarrollo del proyecto se ha utilizado un \textbf{ordenador de sobremesa} con las siguientes características:

\begin{itemize}
\item \textbf{Memoria del sistema}: 2 módulos KHX2666C13 8GiB
\item \textbf{\gls{cpu}}:  AMD Ryzen 5 2600 Six-Core Processor
\item \textbf{\gls{gpu}}: Radeon RX 580 Series
\item \textbf{Almacenamiento}:
\begin{itemize}
\item \textbf{\gls{ssd}} CT500MX500SSD1 500GiB
\item \textbf{\gls{hdd}} ST1000DM010-2EP1 1TiB
\end{itemize}
\end{itemize}
\subsection{Software}
A continuación se enumera el \textbf{software utilizado} para la realización del proyecto:

\begin{itemize}
\item \textbf{Sistema operativo}: Linux Mint 20.1.
\item Entorno de desarrollo \textbf{Eclipse Java EE}.
\item Navegadores \textbf{Mozilla Firefox} y \textbf{Chromium}.
\item Software de control de versiones \textbf{git}.
\item Software \textbf{Dia}, para la creación del diagrama entidad-relación.
\item Software \textbf{Pencil}, para la creación de los mockups.
\item Software \textbf{GanttProject}, para la creación del diagrama de Gantt.
\item \textbf{MySQL Workbench} para el acceso a la base de datos.
\item Cliente \textbf{ssh} para acceso a servidor remoto.
\item \textbf{Texmaker} para la creación de documentos y la memoria.
\item \textbf{AdvancedRestClient}, para la prueba de los endpoints de la aplicación.
\item Software de virtualización \textbf{VirtualBox}, para la creación de servidores virtualizados de prueba.
\end{itemize}

\section{Análisis y diseño}
\subsection{Proceso de análisis}

\subsubsection{Diagrama de casos de uso}
\begin{figure}[H]
	\includegraphics[width=\linewidth]{img/webiblio_diagrama_casos_uso.png}
	\caption{Diagrama de casos de uso del proyecto.}
	\label{fig:casosuso}
\end{figure}

\subsubsection{Diagrama de despliegue}
\begin{figure}[H]
	\includegraphics[width=\linewidth]{img/diagrama_despliegue.png}
	\caption{Diagrama de despliegue del proyecto.}
	\label{fig:despliegue}
\end{figure}

\subsection{Modelo de datos}
\begin{figure}[H]
	\includegraphics[width=\linewidth]{img/diagrama_er.png}
	\caption{Diagrama entidad relación del modelo de datos del proyecto.}
	\label{fig:diagramaer}
\end{figure}

\subsection{Mockups/wireframes}

\begin{figure}[H]
	\includegraphics[width=\linewidth]{img/mockup/home.png}
	\caption{Mockup de la página de inicio.}
	\label{fig:mockhome}
\end{figure}

\begin{figure}[H]
	\includegraphics[width=\linewidth]{img/mockup/ver_libros.png}
	\caption{Mockup de la página de vista de libros.}
	\label{fig:mockverlibros}
\end{figure}

\begin{figure}[H]
	\includegraphics[width=\linewidth]{img/mockup/ver_detalles_libro.png}
	\caption{Mockup de la página de vista de detalles de un libro.}
	\label{fig:mockverdetalleslibro}
\end{figure}

\begin{figure}[H]
	\includegraphics[width=\linewidth]{img/mockup/subir_libro.png}
	\caption{Mockup de la página de subida de libro.}
	\label{fig:mocksubirlibro}
\end{figure}

\begin{figure}[H]
	\includegraphics[width=\linewidth]{img/mockup/perfil.png}
	\caption{Mockup de la página de perfil de usuario.}
	\label{fig:mockuperfil}
\end{figure}

\begin{figure}[H]
	\includegraphics[width=\linewidth]{img/mockup/ver_lista.png}
	\caption{Mockup de la página de vista de lista.}
	\label{fig:mockuplista}
\end{figure}

\begin{figure}[H]
	\includegraphics[width=\linewidth]{img/mockup/libros_pendientes.png}
	\caption{Mockup de la página de libros pendientes de confirmación.}
	\label{fig:mockuplibrospendientes}
\end{figure}

\begin{figure}[H]
	\includegraphics[width=\linewidth]{img/mockup/administrar_usuarios.png}
	\caption{Mockup de la página de administración de usuarios.}
	\label{fig:mockupadminusuarios}
\end{figure}

\begin{figure}[H]
	\includegraphics[width=\linewidth]{img/mockup/administrar_libros.png}
	\caption{Mockup de la página de administración de libros.}
	\label{fig:mockupadminlibros}
\end{figure}


\subsection{Pruebas}
Se han realizado \textbf{pruebas de uso} basándose en el \textbf{diagrama de casos de uso }y asegurándose de que cada rol de usuario solo tenga acceso a sus \textbf{acciones} y pantallas pertinentes, mostrando \textbf{mensajes de error} en caso de que sus permisos no sean suficientes.

En lo que a la \textbf{lógica} de la aplicación web del \textbf{servidor} respecta, se han probado cada uno de los distintos \textbf{\textit{endpoints}} de forma independiente utilizando el software \textit{AdvancedRestClient}, el cual permite realizar peticiones a las distintas URL de la aplicación de forma sencilla y con diversos parámetros.

Para la \textbf{lógica} de la parte del \textbf{cliente}, se ha probado el correcto funcionamiento de las \textbf{acciones y formularios}, tanto en los casos en los que los \textbf{datos de entrada} son \textbf{correctos} como en los casos en los que son \textbf{incorrectos} o faltantes.

\section{Planificación y presupuesto}

\begin{figure}[H]
	\includegraphics[width=\linewidth]{img/diagrama_gantt.png}
	\caption{Diagrama de Gantt del proyecto.}
	\label{fig:gantt}
\end{figure}

La \textbf{planificación} del proyecto se ha realizado según el \textbf{diagrama de Gantt} de la figura \ref{fig:gantt}. En cuanto al número de horas invertidas para la realización completa de la aplicación, se \textbf{estiman aproximadamente 90 horas}.

Para el cálculo del \textbf{presupuesto}, se tendrán en cuenta los recursos humanos y materiales. Respecto a los recursos humanos, se utilizarán las horas estimadas anteriormente y un precio de \textbf{20€/hora} del desarrollador.

Respect a los recursos materiales, se contemplarán los siguientes:

\begin{itemize}
\item \textbf{Material de desarrollo}: material general necesario para el desarrollo, incluyendo el equipo del desarrollador (PC, escritorio, silla, etc.)
\item \textbf{Servidor web}: servidor donde se alojará la aplicación web. Se optará por el alquiler de un servidor web con un precio de \textbf{4€/mes} durante un año.
\end{itemize}

Teniendo esto en cuenta, a continuación se calcula una aproximación del \textbf{presupuesto} del proyecto.

\begin{table}[H]
	\begin{center}
    \begin{tabular}{|l|c|c|} % <-- Alignments: 1st column left, 2nd middle and 3rd right, with vertical lines in between
    		\hline
     	\textbf{Nombre} & \textbf{Tipo} & \textbf{Valor}\\ \hline
      	Desarrollador web & Recurso humano & \textbf{1800€}\\ \hline
      	Material de desarrollo & Recurso material & \textbf{2000€}\\ \hline
      	Servidor para la aplicación & Recurso material & \textbf{48€/año}\\ \hline
      	\multicolumn{2}{|c|}{\textbf{Precio total}} & \textbf{3848€}\\ \hline
    \end{tabular}
  	\end{center}
    \caption{Estimación del presupuesto de la aplicación.}
    \label{tab:presupuesto}
\end{table}


\section{Mejoras y ampliación}
En este apartado se plantean \textbf{cuatro posibles ampliaciones} que podrían realizarse en un futuro pero que no entrarán dentro del proyecto inicial.

\begin{itemize}
\item \textbf{Reseñas de usuarios}: permitir que los usuarios puedan escribir sus propias reseñas e impresiones sobre libros, y 	que estas reseñas aparezcan listas en la página de su respectivo libro.
\item \textbf{Exportar e importar listas}: dar la posibilidad de exportar e importar las listas que los usuarios crean a un formato portable y universal (\gls{xml}, \gls{json}).
\item \textbf{Más funciones sociales}: más funciones sociales para la aplicación, como por ejemplo, un sistema de comentarios en los perfiles de los usuarios o un sistema de me gusta.
\end{itemize}

\section{Conclusiones}
Este proyecto ha resultado muy útil a la hora de \textbf{saber más sobre desarrollo de aplicaciones web} y sobre el \textbf{desarrollo de software} en general. En este proyecto se ha investigado y aprendido mucho sobre \textbf{tecnologías} y \textbf{arquitecturas} usadas en la \textbf{web} a día de hoy, como los \textbf{frameworks \gls{mvc}}, las \gls{api} \gls{rest} y las \textbf{tecnologías asíncronas} en el lado cliente, entre otras.

En este proyecto también se han desarrollado las \textbf{metodologías fundamentales del desarrollo de software} y se ha familiarizado con distintas herramientas usadas en sus distintos apartados: \textbf{análisis, diseño, implementación, documentación, pruebas y
despliegue}.

Además, en este proyecto se ha desarrollado y aplicado el conocimiento sobre las distintas tecnologías y competencias obtenidas en el ciclo.

\newpage
\part{Puesta en producción del proyecto}
\section{Instalación}
\subsection{Prerequisitos}
Antes de comenzar con la instalación de la aplicación web, será necesaria la instalación de \textbf{Java SDK 11}, el gestor de dependencias \textbf{Maven}, el cliente de \textbf{MySQL} y gestor de contenedores \textbf{Docker} en el sistema. La instalación será realizada en un \textbf{sistema Linux}, el cual puede ser instalando utilizando software de virtualización como \textbf{VirtualBox o VMWare}.

Para la instalación de \textbf{Java SDK 11}, ejecutar la siguiente orden en una terminal como administrador:

\begin{lstlisting}[language=bash]
  $ sudo apt install openjdk-11-jdk openjdk-11-jre
\end{lstlisting}

Para la instalación de \textbf{Maven}, ejecutar la siguiente orden en una terminal como administrador:

\begin{lstlisting}[language=bash]
  $ sudo apt install maven
\end{lstlisting}

Para la instalación del \textbf{cliente MySQL}, ejecutar la siguiente orden en una terminal como administrador:

\begin{lstlisting}[language=bash]
  $ sudo apt install mysql-client
\end{lstlisting}

Para la instalación de \textbf{Docker}, ejecutar la siguiente orden en una terminal como administrador:
\begin{lstlisting}[language=bash]
  $ sudo apt install docker.io docker-compose
\end{lstlisting}

Tras la instalación de Docker, se debe añadir el usuario actual del sistema al grupo \textit{docker} y activar el servicio ejecutando los comandos que se mostrarán a continuación. Tras ello, habrá que reiniciar el sistema para que los permisos del nuevo grupo surtan efecto:
\begin{lstlisting}[language=bash]
  $ sudo usermod -a -G docker <usuario>
  $ sudo systemctl enable docker
  $ sudo systemctl start docker
\end{lstlisting}

Una vez realizados todos estos pasos, ya se está listo para comenzar con la instalación de la aplicación en el sistema Linux.

\subsection{Instalación en Linux}

Para la instalación de Linux se seguirán los siguientes pasos:

\begin{enumerate}
	\item Colocarse sobre el directorio raíz del proyecto (\textit{webiblio}) en una terminal (usar la orden \textit{cd}).
	\item Ejecutar la siguiente orden para compilar el \gls{jar} del proyecto: 
	\begin{lstlisting}[language=bash]
  $ mvn clean package
	\end{lstlisting}
	\item Ejecutar la siguiente orden para inicializar el contenedor \textbf{Docker} con la base de datos \textbf{MySQL} en el puerto \textit{3368}:
	\begin{lstlisting}[language=bash]
  $ docker-compose up -d mysql-prod
	\end{lstlisting}
	\item Ejecutar el siguiente comando para \textbf{iniciar la aplicación web} en segundo plano:
	\begin{lstlisting}[language=bash]
  $ java -jar -Dspring.profiles.active=prod
    target/webiblio*.jar &
	\end{lstlisting}
	\item Una vez iniciada la aplicación, ejecutar el siguiente comando para inicializar la base de datos con \textbf{datos de prueba}:
	\begin{lstlisting}[language=bash]
  $ cat sql/libros.sql |docker exec -i mysql-webiblio mysql
    webiblio -u webiblio -pwebiblio
	\end{lstlisting}
\end{enumerate}

Una vez realizados estos pasos, el servidor web escuchará en la URL \textit{http://localhost:8086}, la cual podrá ser utilizada en un \textit{navegador web} para \textit{acceder al servicio web}. Si se quiere parar el servicio en primer plano, se deberá pulsar la combinación de teclas Control+C. Si se quiere volver a ejecutar el servicio una vez ha sido terminado, se tendrá que volver a ejecutar el comando \textit{java} listado anteriormente.

Los \textbf{puertos} en los que escuchan los servicios y la aplicación \textbf{pueden ser cambiados} modificando los\textbf{ archivos de configuración}. Concretamente, los archivos que pueden ser modificados son los siguientes:
\begin{itemize}
	\item \textit{docker-compose.yml}, para los puertos de la base de datos y el visualizador de la base de datos.
	\item \textit{src/main/resources/application-prod.yml}, para los puertos de la aplicación web.
\end{itemize}

Para que los \textbf{cambios} en los puertos surtan efecto, los \textbf{servicios se deben reiniciar} y en caso de la aplicación, \textbf{recompilar}. Para ello, realizar lo siguiente:
\begin{itemize}
	\item \textit{docker-compose.yml}: tras ser editado, ejecutar el siguiente comando para realizar el reinicio. Tras ello, volver a ejecutar los comandos \textbf{\textit{docker-compose up}} del apartado anterior.
	\begin{lstlisting}[language=bash]
  $ docker-compose down
	\end{lstlisting}
	\item\textit{ src/main/resources/application-prod.yml}: tras ser editado, volver a ejecutar los comandos \textit{mvn} y \textit{java} del apartado de instalación.
	
\end{itemize}

Los valores predeterminados de los archivos de configuración pueden ser consultados en el \textbf{anexo}.

\section{Administración}
La aplicación web posee un rol de \textbf{usuario administrador} el cual permite realizar \textbf{tareas administrativas} como la \textbf{creación} y \textbf{borrado} de \textbf{usuarios} y \textbf{libros}. Si se han cargado los datos de prueba en el paso anterior, la aplicación vendrá con un usuarios de rol administrador cuyas \textbf{credenciales} son "admin" (tanto el nombre de usuario como la contraseña). En el \textbf{manual de usuario} se explica de forma más detallada qué funciones pueden realizar los usuarios administradores.

Otra forma de administrar la aplicación consiste en \textbf{conectarse directamente a la base de datos} y gestionar los datos desde ahí. Por defecto, el servidor MySQL escucha en el puerto \textit{3368} y se puede acceder con cualquier \textbf{visualizador de bases de datos} (\textit{phpmyadmin}, \textit{MySQL Workbench}...)

El proyecto utiliza el visualizador de bases de datos \textit{adminer}, una \textbf{interfaz web} para acceder a la base de datos. Para iniciarla, se debe ejecutar el siguiente comando:
\begin{lstlisting}[language=bash]
  $ docker-compose up -d admin
\end{lstlisting}

Una vez ejecutado, la \textbf{interfaz web} podrá ser \textbf{accedida} desde la URL http://localhost:8880. Las \textbf{credenciales} para acceder a la base de datos por defecto del proyecto son las siguientes:
\begin{itemize}
	\item \textbf{Server}: mysql-prod
	\item \textbf{Username}: webiblio
	\item \textbf{Password}: webiblio
	\item \textbf{Database}: webiblio
\end{itemize}

El uso de la interfaz web es \textbf{intuitivo} y \textbf{similar} al resto de visualizadores de bases de datos.

\begin{figure}[H]
	\includegraphics[width=\linewidth]{img/adminer.png}
	\caption{Visualizador de bases de datos adminer.}
	\label{fig:adminer}
\end{figure}

\newpage
\part{Manual de usuario}
\section{Uso de la aplicación}
A continuación se explica el \textbf{funcionamiento básico de la aplicación web} para cada uno de los roles. Esta aplicación cuenta con \textbf{cuatro roles de usuario}, que son los siguientes:
\begin{itemize}
	\item \textbf{Visitante} (usuario no autenticado)
	\item \textbf{Usuario}
	\item \textbf{Moderador}
	\item \textbf{Administrador}
\end{itemize}

Los \textbf{priviliegos} de los roles son \textbf{acumulativos}; es decir que, además de sus propias acciones, un usuario puede realizar todas las acciones de un visitante, un moderador puede realizar todas las acciones de un usuario y un administrador puede realizar todas las acciones de un moderador.

\subsection{Visitante}

Al acceder a la web, un visitante tendrá acceso a la página principal, donde se muestran, entre otras cosas, los \textbf{libros más populares}.
El visitante tiene acceso mediante la barra de navegación a los siguientes menús:
\begin{itemize}
	\item \textbf{Lista de libros}
	\item \textbf{Lista de usuarios con perfil público}
	\item \textbf{Inicio de sesión y registro}
\end{itemize}
\begin{figure}[H]
	\includegraphics[width=\linewidth]{img/usuario/visitante/visitante_home.png}
	\caption{Página de inicio del visitante.}
	\label{fig:visitante_home}
\end{figure}

La \textbf{lista de libros} muestra \textbf{todos los libros disponibles} de la web. Además, ofrece un \textbf{filtro básico} por nombre y un filtro \textbf{avanzado}.

\begin{figure}[H]
	\includegraphics[width=\linewidth]{img/usuario/visitante/visitante_libros.png}
	\caption{Vista de libros.}
	\label{fig:visitante_libros}
\end{figure}

\begin{figure}[H]
	\includegraphics[width=\linewidth]{img/usuario/visitante/visitante_filtro.png}
	\caption{Filtros básico y avanzado en la vista de libros.}
	\label{fig:visitante_filtros}
\end{figure}

Al pulsar en la imagen de un libro o el botón de "Más información", se mostrarán los \textbf{detalles de ese libro}.

\begin{figure}[H]
	\includegraphics[width=\linewidth]{img/usuario/visitante/visitante_verlibro.png}
	\caption{Detalles de un libro.}
	\label{fig:visitante_detalles}
\end{figure}

La \textbf{lista de usuarios} muestra todos los \textbf{usuarios con un perfil público} y un \textbf{enlace} a su perfil.
\begin{figure}[H]
	\includegraphics[width=\linewidth]{img/usuario/visitante/visitante_publicos.png}
	\caption{Listado de usuarios con perfil público.}
	\label{fig:visitante_publicos}
\end{figure}

El \textbf{perfil de un usuario} muestra su nombre y sus \textbf{listas}. Cada lista posee los \textbf{libros que el usuario ha añadido}.

\begin{figure}[H]
	\includegraphics[width=\linewidth]{img/usuario/visitante/visitante_verperfil.png}
	\caption{Vista de un perfil de usuario.}
	\label{fig:visitante_verperfil}
\end{figure}

\begin{figure}[H]
	\includegraphics[width=\linewidth]{img/usuario/visitante/visitante_verlista.png}
	\caption{Vista de una lista de usuario.}
	\label{fig:visitante_verlista}
\end{figure}

Finalmente, un visitante tiene la \textbf{opción de iniciar sesión o registrarse}.

\begin{figure}[H]
	\includegraphics[width=\linewidth]{img/usuario/visitante/visitante_login.png}
	\caption{Inicio de sesión.}
	\label{fig:visitante_login}
\end{figure}

\begin{figure}[H]
	\includegraphics[width=\linewidth]{img/usuario/visitante/visitante_register.png}
	\caption{Registro de usuario.}
	\label{fig:visitante_registro}
\end{figure}


\subsection{Usuario}
Al acceder a la web, un usuario tiene acceso mediante la barra de navegación a los siguientes menús:
\begin{itemize}
	\item \textbf{Lista de libros}
	\item \textbf{Lista de usuarios con perfil público}
	\item \textbf{Subida de libros}
	\item \textbf{Perfil de usuario}
	\item \textbf{Cierre de sesión}
\end{itemize}

\begin{figure}[H]
	\includegraphics[width=\linewidth]{img/usuario/user/user_home.png}
	\caption{Página de inicio del usuario.}
	\label{fig:user_home}
\end{figure}

El usuario tendrá \textbf{acceso a su perfil}, donde podrá realizar las siguientes acciones:

\begin{itemize}
	\item \textbf{Modificar su nivel de privacidad}: si se establece en privado, otros usuarios no podrán ver su perfil.
	\item Vista, creación y eliminación de sus propias \textbf{listas}.
	\item Ver el listado de \textbf{libros favoritos}.
\end{itemize}

\begin{figure}[H]
	\includegraphics[width=\linewidth]{img/usuario/user/user_perfil.png}
	\caption{Perfil del usuario.}
	\label{fig:user_perfil}
\end{figure}

\begin{figure}[H]
	\includegraphics[width=\linewidth]{img/usuario/user/user_favoritos.png}
	\caption{Lista de favoritos del usuario.}
	\label{fig:user_favoritos}
\end{figure}

Dentro de una \textbf{lista}, se pueden ver \textbf{los libros de la misma}, sus \textbf{ratings} y la opción de \textbf{eliminar} libros de esa lista.

\begin{figure}[H]
	\includegraphics[width=\linewidth]{img/usuario/user/user_lista.png}
	\caption{Lista del usuario.}
	\label{fig:user_lista}
\end{figure}

Para \textbf{añadir libros a una lista} y/o añadir ratings y puntuaciones, se debe ir a la \textbf{página de detalles del libro que se quiere añadir}. Si se ha iniciado sesión y se ha creado al menos una lista, aparecerán, además de las \textbf{opciones de ratings y puntuació}n, la opción de \textbf{añadir} ese libro a la lista.

\begin{figure}[H]
	\includegraphics[width=\linewidth]{img/usuario/user/user_libro.png}
	\caption{Ratings y añadir a lista en detalles de libro.}
	\label{fig:user_ratings}
\end{figure}

Finalmente, los usuarios tendrán la opción de \textbf{subir libros} a la página mediante el menú de "Subir libro". Los libros que suban los usuarios rasos se quedarán en un \textbf{estado pendiente} y deberán ser \textbf{confirmados} o \textbf{denegados} por los moderadores o administradores.

Los \textbf{moderadores o administradores} no poseen esta restricción, y los libros que suban serán \textbf{aprobados automáticamente}.

\begin{figure}[H]
	\includegraphics[width=\linewidth]{img/usuario/user/user_subirlibro.png}
	\caption{Formulario de subida de libro.}
	\label{fig:user_subirlibro}
\end{figure}

\subsection{Moderador}
Al acceder a la web, un moderador tiene acceso mediante la barra de navegación a los siguientes menús:
\begin{itemize}
	\item Lista de libros
	\item Lista de usuarios con perfil público
	\item Subida de libros
	\item Perfil de usuario
	\item Panel de administración
	\begin{itemize}
		\item Administrar usuarios
		\item Aprobar o denegar libros pendientes
	\end{itemize}
\end{itemize}

\begin{figure}[H]
	\includegraphics[width=\linewidth]{img/usuario/mod/mod_home.png}
	\caption{Página de inicio del moderador.}
	\label{fig:mod_home}
\end{figure}

En la página \textbf{Administrar usuarios} del panel de administración, los moderadores podrán ver un \textbf{listado de todos los usuarios} con datos detallados. Además, tendrán la opción de \textbf{habilitar} o \textbf{deshabilitar} un usuario. Un usuario deshabilitado no podrá iniciar sesión en la web.

\begin{figure}[H]
	\includegraphics[width=\linewidth]{img/usuario/mod/mod_usuarios.png}
	\caption{Administración de usuarios del moderador.}
	\label{fig:mod_usuarios}
\end{figure}

En la página\textbf{ Libros pendientes} del panel de administración, los moderadores podrán ver un listado de todos los libros que han sido subidos y que están \textbf{pendientes de ser aprobados o denegados}. Podrán \textbf{aprobar} estos libros, lo cual hará que sean visibles para todos los usuarios de aplicación o \textbf{denegarlos}, lo cual hará que no sean visibles para los usuarios de la aplicación.

\begin{figure}[H]
	\includegraphics[width=\linewidth]{img/usuario/mod/mod_pendientes.png}
	\caption{Página de libros pendientes de ser aprobados o denegados.}
	\label{fig:mod_pendientes}
\end{figure}

\subsection{Administrador}
Al acceder a la web, un administrador tiene acceso mediante la barra de navegación a los siguientes menús:
\begin{itemize}
	\item Lista de libros
	\item Lista de usuarios con perfil público
	\item Subida de libros
	\item Perfil de usuario
	\item Panel de administración
	\begin{itemize}
		\item Administrar usuarios
		\item Administar libros
		\item Aprobar o denegar libros pendientes
	\end{itemize}
\end{itemize}

\begin{figure}[H]
	\includegraphics[width=\linewidth]{img/usuario/admin/admin_home.png}
	\caption{Página de inicio del administrador.}
	\label{fig:admin_home}
\end{figure}

En la página \textbf{Administrar usuarios} del panel de administración, los administrador podrán ver un \textbf{listado de todos los usuarios} con datos detallados. Además de las opciones del moderador, los administradores podrán\textbf{ crear y eliminar usuarios}.

\begin{figure}[H]
	\includegraphics[width=\linewidth]{img/usuario/admin/admin_usuarios.png}
	\caption{Panel de administración de usuarios del administrador.}
	\label{fig:admin_usuarios}
\end{figure}

En la página \textbf{Administrar libros} del panel de administración, los administrador podrán ver un \textbf{listado de todos los libros}. Además, los administradores podrán \textbf{eliminar libros}.

\begin{figure}[H]
	\includegraphics[width=\linewidth]{img/usuario/admin/admin_libros.png}
	\caption{Panel de administración de libros del administrador.}
	\label{fig:admin_libros}
\end{figure}

\newpage
\part{Bibliografía}
\bibliographystyle{IEEEtran}
\begin{thebibliography}{9}
\bibitem{thymeleaf}
The Thymeleaf Team. "Documentation - Tymeleaf". Thymeleaf.
\\\texttt{https://www.thymeleaf.org/documentation.html} (\textbf{acceso}: 5 de junio de 2021)

\bibitem{spring}
The Spring Team. "Spring | Projects". Spring.
\\\texttt{https://spring.io/projects} (\textbf{acceso}: 5 de junio de 2021)

\bibitem{docker}
Docker Inc. "Reference documentation | Docker documentation". Docker.
\\\texttt{https://docs.docker.com/reference/} (\textbf{acceso}: 5 de junio de 2021)

\bibitem{latex}
LaTeX Project. "LaTeX Documentation". LaTeX.
\\\texttt{https://www.latex-project.org/help/documentation/} (\textbf{acceso}: 5 de junio de 2021)

\bibitem{designpatterns} 
E. Gamma, R. Helm, R. Johnson y J. Vlissides.
\textit{Design Patterns: Elements of Reusable Object-Oriented Software}.
Indiana: Addison-Wesley, 2016. 
\end{thebibliography}

\newpage
\part{Anexo}
\section{Archivos de configuración}
\subsection{Archivo de configuración predeterminado de la base de datos}
\textit{docker-compose.yml}

\begin{lstlisting}
version: '3'

services:

  mysql-prod:
    image: mysql:8.0.23
    container_name: mysql-webiblio
    volumes:
      - webiblio-data:/data/db
    environment:
      MYSQL_USER: webiblio
      MYSQL_PASSWORD: webiblio
      MYSQL_ROOT_PASSWORD: webiblio
      MYSQL_DATABASE: webiblio
    ports:
      - "3368:3306"

  admin:    
    image: adminer
    container_name: adminer-webiblio
    ports:      
      - "8880:8080"
volumes:
  webiblio-data:
\end{lstlisting}

\subsection{Archivo de configuración de la aplicación web}
\textit{src/main/resources/application-prod.yml}

\begin{lstlisting}
spring:
  datasource:
    url: jdbc:mysql://localhost:3368/webiblio
    username: webiblio
    password: webiblio
  jpa:
    generate-ddl: true
server:
  port: 8086
\end{lstlisting}

\end{document}

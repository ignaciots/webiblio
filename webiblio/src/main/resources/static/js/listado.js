// Toggle de menú de búsqueda avanzada.
function toggleBusquedaAvanzada() {
	const divBusquedaAvanzada = document
		.querySelector("#divBusquedaAvanzada");
	divBusquedaAvanzada.hidden = !divBusquedaAvanzada.hidden;
}

const divBusqueda = document.querySelector("#btnBusquedaAvanzada");
if (divBusqueda != null) {
	divBusqueda.onclick = toggleBusquedaAvanzada;
}

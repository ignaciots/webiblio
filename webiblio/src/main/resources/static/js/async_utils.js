/**
 * Realiza una llamada asíncrona a una URL de la API.
 * @param {URL completa de la API} apiPath 
 * @param {Elemento a mostrar cuando la llamada tiene éxito.} domSuccess 
 * @param {Elemento a mostrar cuando la llamada fall.} domError 
 * @param {Realiza o no acción sobre el DOM al tener éxito (predeterminado, true)} handleSuccess 
 * @returns 
 */
async function apiAsyncIdAndParam(apiPath, domSuccess, domError, handleSuccess = true) {
	let response;
	try {
		response = await fetch(apiPath);
	} catch (e) {
		// En error, mostrar DOM de error.
		domSuccess.hidden = true;
		domError.hidden = false;
	}

	if (response === undefined || !response.ok) {
		// Si no hay respuesta o no es correcta, mostrar DOM de error.
		domSuccess.hidden = true;
		domError.hidden = false;
	} else if (handleSuccess) {
		domSuccess.hidden = false;
		domError.hidden = true;
	}

	// Valores de retorno al finalizar
	if (response === undefined) {
		return null;
	}
	return response.json();
}

/**
 * Realiza una llamada asíncrona a una URL de la API que retorna un valor booleano.
 * @param {URL completa de la API} apiPath 
 * @param {Elemento a mostrar cuando la llamada tiene éxito.} domSuccess 
 * @param {Elemento a mostrar cuando la llamada falla.} domError 
 * @param {Elemento a mostrar cuando la llamada tiene éxito pero no es correcta.} domFail
 */
async function apiAsyncIdAndParamWithReturn(apiPath, domSuccess, domError, domFail) {
	let hideDomFail = true;
	let response = await apiAsyncIdAndParam(apiPath, domSuccess, domError, false);
	if (response != null) {
		if (response === true) {
			domSuccess.hidden = false;
			domError.hidden = true;
		} else {
			domSuccess.hidden = true;
			domError.hidden = true;
			hideDomFail = false;
		}
	}

	domFail.hidden = hideDomFail;
}
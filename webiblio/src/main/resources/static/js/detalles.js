// Puntuación
const btnPuntuacion = document.querySelector("#btnPuntuacion");

loadOnclickEvent(btnPuntuacion, async () => {
	const apiId= btnPuntuacion.dataset.idLibro;
	const apiParam = "puntuacion";
	const apiParamValue = document.querySelector("#selectPuntuacion").value;
	apiAsyncIdAndParam(`/ratings/puntuacion/${apiId}?${apiParam}=${apiParamValue}`,
	document.querySelector("#puntuacionSuccess"),
	document.querySelector("#puntuacionError"));
});

// Estado
const btnEstado = document.querySelector("#btnEstado");
loadOnclickEvent(btnEstado, async () => {
	const apiId = btnEstado.dataset.idLibro;
	const apiParam = "estadoLectura";
	const apiParamValue = document.querySelector("#selectEstadoLibro").value;
	apiAsyncIdAndParam(`/ratings/estado/${apiId}?${apiParam}=${apiParamValue}`,
	document.querySelector("#estadoSuccess"),
	document.querySelector("#estadoError"));
});

// Añadir a lista
const btnAddLista = document.querySelector("#btnAddLista");
loadOnclickEvent(btnAddLista, async() => {
	const apiId = btnAddLista.dataset.idLibro;
	const apiParam = "idLista";
	const apiParamValue = document.querySelector("#selectIdLista").value;
	apiAsyncIdAndParamWithReturn(`/listas/add/${apiId}?${apiParam}=${apiParamValue}`,
	document.querySelector("#addListaSuccess"),
	document.querySelector("#addListaError"),
	document.querySelector("#addListaFail"));
});
// Comprobación de contraseña introducida
function comprobarPasswords() {
	let password1 = document.querySelector("#inputPassword1");
	let password2 = document.querySelector("#inputPassword2");
	if (password1.value === password2.value) {
		password1.setCustomValidity("");
	} else {
		password1.setCustomValidity("Las contraseñas no coinciden.");
	}
	password1.reportValidity();
}

const btnRegistro = document.querySelector("#buttonSubmitRegistro");
if (btnRegistro != null) {
	btnRegistro.onclick = function() {
		comprobarPasswords();
	};
}

// Nivel de privacidad
const btnNivelPrivacidad = document.querySelector("#btnNivelPrivacidad");
const apiId = btnNivelPrivacidad.dataset.idUsuario;
const apiParam = "nivelPrivacidad";

loadOnclickEvent(btnNivelPrivacidad, async () => {
	const apiParamValue = document.querySelector("#selectNivelPrivacidad").value;
	apiAsyncIdAndParam(`/perfil/privacidad/${apiId}?${apiParam}=${apiParamValue}`,
	document.querySelector("#nivelPrivacidadSuccess"),
	document.querySelector("#nivelPrivacidadError"));
});

// Crear lista
const btnCrearLista = document.querySelector("#btnCrearLista");

loadOnclickEvent(btnCrearLista, async () => {
	const lista = document.querySelector("#inputNombreLista");
	const nombreLista = lista.value;
	if (nombreLista.trim().length === 0) {
		lista.setCustomValidity("El valor no debe estar en blanco.");
	} else {
		lista.setCustomValidity("");
	}
});
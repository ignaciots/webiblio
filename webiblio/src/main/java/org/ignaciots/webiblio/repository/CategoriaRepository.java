package org.ignaciots.webiblio.repository;

import java.util.Optional;

import org.ignaciots.webiblio.model.Categoria;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoriaRepository extends JpaRepository<Categoria, Long> {

	public Optional<Categoria> findByNombre(String nombre);
}

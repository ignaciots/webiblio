package org.ignaciots.webiblio.repository;

import java.util.Optional;

import org.ignaciots.webiblio.model.EstadoUsuario;
import org.ignaciots.webiblio.model.RolUsuario;
import org.ignaciots.webiblio.model.Usuario;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

	public Optional<Usuario> findByNombreUsuario(String nombreUsuario);

	public Optional<Usuario> findByIdAndEstado(Long idUsuario, EstadoUsuario estado);

	public Page<Usuario> findByRolAndNombreUsuarioNot(RolUsuario rol, String nombreUsuario, Pageable paging);

	public Page<Usuario> findByNombreUsuarioNot(String nombreUsuario, Pageable paging);

	@Query(value = "SELECT usuario.* FROM usuario LEFT JOIN lista ON usuario.id = lista.id_usuario WHERE lista.id = ?1", nativeQuery = true)
	public Optional<Usuario> findByIdLista(Long idLista);

	@Query(value = "SELECT usuario.* FROM usuario WHERE nivel_privacidad = ?1 AND nombre_usuario LIKE %?2%", countQuery = "SELECT COUNT(usuario.id) FROM usuario WHERE nivel_privacidad = ?1 AND nombre_usuario LIKE %?2%", nativeQuery = true)
	public Page<Usuario> findByNivelPrivacidadAndNombreUsuarioLike(String nivelPrivacidad, String nombreUsuario,
			Pageable pageable);
}

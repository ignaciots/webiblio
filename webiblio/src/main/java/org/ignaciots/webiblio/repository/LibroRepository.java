package org.ignaciots.webiblio.repository;

import java.util.List;
import java.util.Optional;

import org.ignaciots.webiblio.model.EstadoLibro;
import org.ignaciots.webiblio.model.Libro;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface LibroRepository extends JpaRepository<Libro, Long> {
	Page<Libro> findAllByAprobado(EstadoLibro aprobado, Pageable paging);

	List<Libro> findByAprobado(EstadoLibro aprobado);

	Page<Libro> findByAprobado(EstadoLibro aprobado, Pageable pageable);

	@Query(value = "SELECT libro.* FROM (SELECT id_libro, COUNT(favorito) as favoritos FROM rating_usuario_libro WHERE favorito=1 GROUP BY id_libro) as favslibro RIGHT JOIN  libro ON favslibro.id_libro = libro.id WHERE libro.aprobado=1 ORDER BY favoritos DESC LIMIT 6", nativeQuery = true)
	List<Libro> findAllOrderedByPopular();

	Optional<Libro> findByIdAndAprobado(Long id, EstadoLibro aprobado);

	@Query(value = "SELECT DISTINCT libro.* FROM libro LEFT JOIN libro_categoria ON libro.id = libro_categoria.id_libro  LEFT JOIN categoria ON libro_categoria.id_categoria = categoria.id WHERE libro.aprobado=1 AND libro.titulo LIKE %?1% AND libro.autores LIKE %?2% AND categoria.nombre LIKE %?3%", nativeQuery = true, countQuery = "SELECT count(*) FROM (SELECT DISTINCT libro.* FROM libro LEFT JOIN libro_categoria ON libro.id = libro_categoria.id_libro  LEFT JOIN categoria ON libro_categoria.id_categoria = categoria.id WHERE libro.aprobado=1 AND libro.titulo LIKE %?1% AND libro.autores LIKE %?2% AND categoria.nombre LIKE %?3%) F")
	Page<Libro> findByNombreAutoresCategoriaAndAprobado(String titulo, String autores, String categoria,
			Pageable pageable);

	@Query(value = "SELECT next_val FROM hibernate_sequence", nativeQuery = true)
	Integer findHibernateNextVal();

	@Query(value = "SELECT DISTINCT libro.* FROM libro LEFT JOIN rating_usuario_libro ON libro.id = rating_usuario_libro.id_libro WHERE rating_usuario_libro.id_usuario = ?1 AND rating_usuario_libro.favorito = 1", nativeQuery = true, countQuery = "SELECT count(*) FROM (SELECT DISTINCT libro.* FROM libro LEFT JOIN rating_usuario_libro ON libro.id = rating_usuario_libro.id_libro WHERE rating_usuario_libro.id_usuario = ?1 AND rating_usuario_libro.favorito = 1) libros")
	public Page<Libro> findLibrosFavoritos(Long idUsuario, Pageable pageable);
}

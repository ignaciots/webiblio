package org.ignaciots.webiblio.repository;

import org.ignaciots.webiblio.model.ClaveRatingUsuarioLibro;
import org.ignaciots.webiblio.model.RatingUsuarioLibro;
import org.ignaciots.webiblio.projections.EntradaListaProjection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface RatingUsuarioLibroRepository extends JpaRepository<RatingUsuarioLibro, ClaveRatingUsuarioLibro> {

	@Query(value = "SELECT f.libroid as id, f.librotitulo as titulo, r.estado as estado, r.favorito as favorito, r.puntuacion as puntuacion FROM rating_usuario_libro r RIGHT JOIN \n"
			+ "(SELECT libro.id as libroid, libro.titulo as librotitulo, lista.id_usuario as usuarioid  FROM libro RIGHT JOIN lista_libro ON libro.id=lista_libro.id_libro LEFT JOIN lista ON lista.id = lista_libro.id_lista WHERE lista_libro.id_lista = ?1) f\n"
			+ "ON f.libroid=r.id_libro\n"
			+ "AND f.usuarioid = r.id_usuario", countQuery = "SELECT COUNT(f.libroid) FROM rating_usuario_libro r RIGHT JOIN  (SELECT libro.id as libroid, libro.titulo as librotitulo, lista.id_usuario as usuarioid  FROM libro RIGHT JOIN lista_libro ON libro.id=lista_libro.id_libro LEFT JOIN lista ON lista.id = lista_libro.id_lista WHERE lista_libro.id_lista = ?1) f ON f.libroid=r.id_libro AND f.usuarioid = r.id_usuario", nativeQuery = true)
	public Page<EntradaListaProjection> findByIdLista(Long idLista, Pageable pageable);
}

package org.ignaciots.webiblio.repository;

import java.util.List;

import org.ignaciots.webiblio.model.Lista;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ListaRepository extends JpaRepository<Lista, Long> {

	@Query(value = "SELECT lista.* FROM lista WHERE id_usuario = ?1", nativeQuery = true)
	public List<Lista> findByIdUsuario(Long idUsuario);
	
	@Query(value = "SELECT lista.* FROM lista LEFT JOIN lista_libro ON lista_libro.id_lista=lista.id WHERE lista_libro.id_libro = ?1", nativeQuery = true)
	public List<Lista> findByIdLibro(Long idUsuario);
}

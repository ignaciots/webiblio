package org.ignaciots.webiblio.exceptions;

/**
 * Excepción que indica que un usuario ya existe.
 *
 * @author Ignacio Torre Suárez
 *
 */
public class UsuarioYaExisteException extends Exception {

	private static final long serialVersionUID = 4746199088357469944L;

	public UsuarioYaExisteException() {
	}

	public UsuarioYaExisteException(String message) {
		super(message);
	}

	public UsuarioYaExisteException(String message, Throwable cause) {
		super(message, cause);
	}

	public UsuarioYaExisteException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public UsuarioYaExisteException(Throwable cause) {
		super(cause);
	}

}

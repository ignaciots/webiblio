package org.ignaciots.webiblio.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
/**
 * Configuración de la aplicación
 *
 * @author Ignacio Torre Suárez
 *
 */
public class WebiblioMvcConfig implements WebMvcConfigurer {
	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/login").setViewName("auth/login");
		registry.addViewController("/logout").setViewName("auth/logout");
	}
}

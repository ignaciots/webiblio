package org.ignaciots.webiblio.model;

public enum EstadoLectura {
	PLAN_LEER("Por leer"), LEYENDO("Leyendo"), LEIDO("Leído");

	private String prettyName;

	private EstadoLectura(String prettyName) {
		this.prettyName = prettyName;
	}

	@Override
	public String toString() {
		return prettyName;
	}
}

package org.ignaciots.webiblio.model;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Embeddable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Embeddable
@Getter
@Setter
@ToString
public class ClaveRatingUsuarioLibro implements Serializable {

	private static final long serialVersionUID = -1774753970277391513L;

	private Long idUsuario;

	private Long idLibro;

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ClaveRatingUsuarioLibro other = (ClaveRatingUsuarioLibro) obj;
		return Objects.equals(idLibro, other.idLibro) && Objects.equals(idUsuario, other.idUsuario);
	}

	@Override
	public int hashCode() {
		return Objects.hash(idLibro, idUsuario);
	}
}

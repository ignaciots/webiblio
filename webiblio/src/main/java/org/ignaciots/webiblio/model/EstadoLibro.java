package org.ignaciots.webiblio.model;

public enum EstadoLibro {
	POR_APROBAR("Por aprobar"), APROBADO("Aprobado"), DENEGADO("Denegado");

	private String prettyName;

	private EstadoLibro(String prettyName) {
		this.prettyName = prettyName;
	}

	@Override
	public String toString() {
		return prettyName;
	}

}

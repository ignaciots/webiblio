package org.ignaciots.webiblio.model;

import java.util.Objects;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
public class RatingUsuarioLibro {

	@EmbeddedId
	private ClaveRatingUsuarioLibro id = new ClaveRatingUsuarioLibro();

	@ManyToOne
	@MapsId("idUsuario")
	@JoinColumn(name = "idUsuario")
	private Usuario usuario;

	@ManyToOne
	@MapsId("idLibro")
	@JoinColumn(name = "idLibro")
	private Libro libro;

	@Enumerated(EnumType.STRING)
	private EstadoLectura estado;

	@Enumerated(EnumType.ORDINAL)
	private PuntuacionLibro puntuacion;

	private Boolean favorito = false;

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		RatingUsuarioLibro other = (RatingUsuarioLibro) obj;
		return Objects.equals(id, other.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
}

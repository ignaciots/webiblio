package org.ignaciots.webiblio.model;

public enum EstadoUsuario {
	HABILITADO("Habilitado"), DESHABILITADO("Deshabilitado");

	private String prettyName;

	private EstadoUsuario(String prettyName) {
		this.prettyName = prettyName;
	}

	@Override
	public String toString() {
		return prettyName;
	}
}

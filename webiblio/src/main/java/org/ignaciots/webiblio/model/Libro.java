package org.ignaciots.webiblio.model;

import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
public class Libro {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(nullable = false)
	private String titulo;

	@Column(length = 1000, nullable = false)
	private String sinopsis;

	@Column(nullable = false)
	private Integer anyoPublicacion;

	@Column(nullable = false)
	private Integer numeroPaginas;

	@Column(nullable = false)
	private String autores;

	@Column(nullable = false)
	private String pathLibro;

	@Column(nullable = false)
	private String pathImagen;

	@Column(nullable = false)
	@Enumerated(EnumType.ORDINAL)
	private EstadoLibro aprobado = EstadoLibro.POR_APROBAR;

	@ManyToMany
	@JoinTable(name = "LibroCategoria", joinColumns = @JoinColumn(name = "idLibro"), inverseJoinColumns = @JoinColumn(name = "idCategoria"))
	private Set<Categoria> categorias;

	@ManyToMany(mappedBy = "libros")
	private Set<Lista> listas;

	@OneToMany(mappedBy = "libro")
	private Set<RatingUsuarioLibro> ratings;

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Libro other = (Libro) obj;
		return Objects.equals(id, other.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
}

package org.ignaciots.webiblio.model;

import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
public class Usuario {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(length = 50, nullable = false, unique = true)
	@Size(max = 50, min = 3)
	@NotBlank
	private String nombreUsuario;

	@Column(nullable = false, length = 512)
	@Size(max = 512, min = 5)
	@NotBlank
	private String password;

	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private EstadoUsuario estado = EstadoUsuario.DESHABILITADO;

	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private RolUsuario rol = RolUsuario.USUARIO;

	@Column(nullable = true)
	@Enumerated(EnumType.STRING)
	private NivelPrivacidad nivelPrivacidad = NivelPrivacidad.PRIVADO;

	@OneToMany(mappedBy = "usuario")
	private Set<Lista> listas;

	@OneToMany(mappedBy = "usuario")
	private Set<RatingUsuarioLibro> ratings;

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Usuario other = (Usuario) obj;
		return Objects.equals(id, other.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
}

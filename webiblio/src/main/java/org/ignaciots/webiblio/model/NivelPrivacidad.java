package org.ignaciots.webiblio.model;

public enum NivelPrivacidad {

	PUBLICO("Público"), PRIVADO("Privado");

	private String prettyName;

	private NivelPrivacidad(String prettyName) {
		this.prettyName = prettyName;
	}

	@Override
	public String toString() {
		return prettyName;
	}

}

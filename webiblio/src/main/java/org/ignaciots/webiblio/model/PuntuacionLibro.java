package org.ignaciots.webiblio.model;

public enum PuntuacionLibro {
	UNA_ESTRELLA("\u2605"), DOS_ESTRELLAS("\u2605\u2605"), TRES_ESTRELLAS("\u2605\u2605\u2605"),
	CUATRO_ESTRELLAS("\u2605\u2605\u2605\u2605"), CINCO_ESTRELLAS("\u2605\u2605\u2605\u2605\u2605");

	private String prettyName;

	private PuntuacionLibro(String prettyName) {
		this.prettyName = prettyName;
	}

	@Override
	public String toString() {
		return prettyName;
	}
}

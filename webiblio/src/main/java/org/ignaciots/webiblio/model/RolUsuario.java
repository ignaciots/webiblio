package org.ignaciots.webiblio.model;

public enum RolUsuario {
	USUARIO("Usuario"), MODERADOR("Moderador"), ADMINISTRADOR("Administrador");

	private String prettyName;

	private RolUsuario(String prettyName) {
		this.prettyName = prettyName;
	}

	@Override
	public String toString() {
		return prettyName;
	}
}

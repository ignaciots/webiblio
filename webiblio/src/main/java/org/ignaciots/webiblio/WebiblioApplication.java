package org.ignaciots.webiblio;

import org.ignaciots.webiblio.storage.StorageProperties;
import org.ignaciots.webiblio.storage.StorageService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableConfigurationProperties(StorageProperties.class)
public class WebiblioApplication {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		SpringApplication.run(WebiblioApplication.class, args);
	}

	@Bean
	CommandLineRunner init(StorageService storageService) {
		return args -> {
			storageService.deleteAll();
			storageService.init();
		};
	}

}

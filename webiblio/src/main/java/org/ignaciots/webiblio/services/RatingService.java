package org.ignaciots.webiblio.services;

import java.util.Optional;

import org.ignaciots.webiblio.model.ClaveRatingUsuarioLibro;
import org.ignaciots.webiblio.model.EstadoLectura;
import org.ignaciots.webiblio.model.Libro;
import org.ignaciots.webiblio.model.PuntuacionLibro;
import org.ignaciots.webiblio.model.RatingUsuarioLibro;
import org.ignaciots.webiblio.model.Usuario;
import org.ignaciots.webiblio.projections.EntradaListaProjection;
import org.ignaciots.webiblio.repository.RatingUsuarioLibroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class RatingService {

	@Autowired
	private RatingUsuarioLibroRepository ratingRepo;

	public void toggleFavorito(Libro libro, Usuario usuario) {
		var claveRating = new ClaveRatingUsuarioLibro();
		claveRating.setIdLibro(libro.getId());
		claveRating.setIdUsuario(usuario.getId());
		var rating = ratingRepo.findById(claveRating);

		var r = rating.orElse(new RatingUsuarioLibro());
		r.setLibro(libro);
		r.setUsuario(usuario);
		// Si no tiene datos, meter en favorito
		r.setFavorito(r.getFavorito() == null ? true : !r.getFavorito());
		ratingRepo.save(r);
	}

	public boolean esFavorito(Usuario usuario, Libro libro) {
		var claveRating = new ClaveRatingUsuarioLibro();
		claveRating.setIdLibro(libro.getId());
		claveRating.setIdUsuario(usuario.getId());
		var rating = ratingRepo.findById(claveRating);

		if (rating.isPresent()) {
			return rating.get().getFavorito();
		}
		return false;
	}

	public void guardarPuntuacion(Libro libro, Usuario usuario, PuntuacionLibro puntuacion) {
		var claveRating = new ClaveRatingUsuarioLibro();
		claveRating.setIdLibro(libro.getId());
		claveRating.setIdUsuario(usuario.getId());
		var rating = ratingRepo.findById(claveRating);

		var r = rating.orElse(new RatingUsuarioLibro());
		r.setLibro(libro);
		r.setUsuario(usuario);
		r.setPuntuacion(puntuacion);
		ratingRepo.save(r);

	}

	public Optional<PuntuacionLibro> getPuntuacion(Usuario usuario, Libro libro) {
		var claveRating = new ClaveRatingUsuarioLibro();
		claveRating.setIdLibro(libro.getId());
		claveRating.setIdUsuario(usuario.getId());
		var rating = ratingRepo.findById(claveRating);

		if (rating.isPresent()) {
			return Optional.ofNullable(rating.get().getPuntuacion());
		}
		return Optional.empty();
	}

	public void guardarEstadoLectura(Libro libro, Usuario usuario, EstadoLectura estadoLectura) {
		var claveRating = new ClaveRatingUsuarioLibro();
		claveRating.setIdLibro(libro.getId());
		claveRating.setIdUsuario(usuario.getId());
		var rating = ratingRepo.findById(claveRating);

		var r = rating.orElse(new RatingUsuarioLibro());
		r.setLibro(libro);
		r.setUsuario(usuario);
		r.setEstado(estadoLectura);
		ratingRepo.save(r);
	}

	public Optional<EstadoLectura> getEstadoLectura(Usuario usuario, Libro libro) {
		var claveRating = new ClaveRatingUsuarioLibro();
		claveRating.setIdLibro(libro.getId());
		claveRating.setIdUsuario(usuario.getId());
		var rating = ratingRepo.findById(claveRating);

		if (rating.isPresent()) {
			return Optional.ofNullable(rating.get().getEstado());
		}
		return Optional.empty();
	}

	public Page<EntradaListaProjection> getRatingsByLista(Long idLista, Integer page, Integer size) {
		Pageable pageable = PageRequest.of(page, size);
		return ratingRepo.findByIdLista(idLista, pageable);
	}
}

package org.ignaciots.webiblio.services;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

import org.ignaciots.webiblio.exceptions.UsuarioYaExisteException;
import org.ignaciots.webiblio.model.EstadoUsuario;
import org.ignaciots.webiblio.model.NivelPrivacidad;
import org.ignaciots.webiblio.model.RolUsuario;
import org.ignaciots.webiblio.model.Usuario;
import org.ignaciots.webiblio.repository.ListaRepository;
import org.ignaciots.webiblio.repository.RatingUsuarioLibroRepository;
import org.ignaciots.webiblio.repository.UsuarioRepository;
import org.ignaciots.webiblio.util.AuthUtil;
import org.ignaciots.webiblio.util.CryptoUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class UsuarioService {

	@Autowired
	private UsuarioRepository userRepo;

	@Autowired
	private RatingUsuarioLibroRepository ratingsRepo;

	@Autowired
	private ListaRepository listRepo;

	public void cambiarNivelPrivacidad(Usuario usuario, String nivelPrivacidad) {
		if (!Arrays.stream(NivelPrivacidad.values()).map(NivelPrivacidad::name).collect(Collectors.toList())
				.contains(nivelPrivacidad)) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Nivel de privacidad no válido.");
		}
		usuario.setNivelPrivacidad(NivelPrivacidad.valueOf(nivelPrivacidad));
		userRepo.save(usuario);
	}

	public Optional<Usuario> getUsuarioSiActivo(Long idUsuario) {
		return userRepo.findByIdAndEstado(idUsuario, EstadoUsuario.HABILITADO);
	}

	public Optional<Usuario> getUsuarioActual() {
		var usuario = AuthUtil.getUsuarioActual();
		if (usuario instanceof String) {
			return Optional.empty();
		}

		return userRepo.findByNombreUsuario(((User) usuario).getUsername());
	}

	public void registrarUsuario(Usuario usuario) throws UsuarioYaExisteException {
		if (userRepo.findByNombreUsuario(usuario.getNombreUsuario()).isPresent()) {
			throw new UsuarioYaExisteException();
		}

		usuario.setPassword(CryptoUtil.getBCryptPasswordEncoder().encode(usuario.getPassword()));
		usuario.setEstado(EstadoUsuario.HABILITADO);
		usuario.setRol(RolUsuario.USUARIO);

		userRepo.save(usuario);
	}

	public void crearUsuario(Usuario usuario) throws UsuarioYaExisteException {
		if (userRepo.findByNombreUsuario(usuario.getNombreUsuario()).isPresent()) {
			throw new UsuarioYaExisteException();
		}

		usuario.setPassword(CryptoUtil.getBCryptPasswordEncoder().encode(usuario.getPassword()));

		userRepo.save(usuario);
	}

	public Page<Usuario> getUsuariosRasos(Integer pageNumber, Integer pageSize) {
		Pageable paging = PageRequest.of(pageNumber, pageSize);
		// Se excluye a sí mismo
		return userRepo.findByRolAndNombreUsuarioNot(RolUsuario.USUARIO, getUsuarioActual().get().getNombreUsuario(),
				paging);
	}

	public Page<Usuario> getUsuarios(Integer pageNumber, Integer pageSize) {
		Pageable paging = PageRequest.of(pageNumber, pageSize);
		// Se excluye a sí mismo
		return userRepo.findByNombreUsuarioNot(getUsuarioActual().get().getNombreUsuario(), paging);
	}

	public Page<Usuario> buscarUsuariosPublicos(String nombreUsuario, Integer pageNumber, Integer pageSize) {
		Pageable pageable = PageRequest.of(pageNumber, pageSize);
		return userRepo.findByNivelPrivacidadAndNombreUsuarioLike(NivelPrivacidad.PUBLICO.name(), nombreUsuario,
				pageable);
	}

	public boolean puedeAccederPerfil(String nombreUsuario, NivelPrivacidad nivelPrivacidad) {
		if (nivelPrivacidad == NivelPrivacidad.PUBLICO) {
			return true;
		}
		return AuthUtil.esMismoUsuario(nombreUsuario);
	}

	public void deleteUser(Usuario user) {
		listRepo.deleteAll(user.getListas());
		ratingsRepo.deleteAll(user.getRatings());
		userRepo.delete(user);
	}

}

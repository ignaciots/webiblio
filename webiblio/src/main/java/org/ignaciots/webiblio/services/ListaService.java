package org.ignaciots.webiblio.services;

import java.util.ArrayList;
import java.util.List;

import org.ignaciots.webiblio.model.Lista;
import org.ignaciots.webiblio.repository.ListaRepository;
import org.ignaciots.webiblio.repository.UsuarioRepository;
import org.ignaciots.webiblio.util.AuthUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ListaService {

	@Autowired
	private ListaRepository listRepo;

	@Autowired
	private UsuarioRepository userRepo;

	@Autowired
	private UsuarioService userService;

	private Logger logger = LoggerFactory.getLogger(LibroService.class);

	public List<Lista> getListasUsuario() {
		if (!AuthUtil.estaUsuarioAutenticado()) {
			return new ArrayList<>();
		}

		var usuarioActual = userService.getUsuarioActual();
		if (usuarioActual.isEmpty()) {
			return new ArrayList<>();
		}

		return listRepo.findByIdUsuario(usuarioActual.get().getId());
	}

	public boolean perteneceUsuarioActual(Long idLista) {
		if (!AuthUtil.estaUsuarioAutenticado()) {
			logger.debug("La lista {} no pertenece al usuario actual.", idLista);
			return false;
		}
		var lista = listRepo.findById(idLista);
		if (!lista.isPresent()) {
			logger.debug("La lista {} no pertenece al usuario actual.", idLista);
			return false;
		}
		var usuario = userService.getUsuarioActual();
		return usuario.get().getListas().contains(lista.get());
	}

	public boolean puedeAccederLista(Long idLista) {
		var usuario = userRepo.findByIdLista(idLista);
		if (!usuario.isPresent()) {
			logger.debug("No se puede acceder a la lista {}", idLista);
			return false;
		}

		return userService.puedeAccederPerfil(usuario.get().getNombreUsuario(), usuario.get().getNivelPrivacidad());
	}
}

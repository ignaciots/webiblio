package org.ignaciots.webiblio.services;

import java.util.Optional;

import org.ignaciots.webiblio.model.EstadoLibro;
import org.ignaciots.webiblio.model.Libro;
import org.ignaciots.webiblio.repository.LibroRepository;
import org.ignaciots.webiblio.repository.ListaRepository;
import org.ignaciots.webiblio.repository.RatingUsuarioLibroRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class LibroService {
	@Autowired
	private LibroRepository bookRepo;

	@Autowired
	private ListaRepository listRepo;
	@Autowired
	private RatingUsuarioLibroRepository ratingRepo;

	private Logger logger = LoggerFactory.getLogger(LibroService.class);

	public Page<Libro> buscarLibrosAprobados(Integer pageNumber, Integer pageSize, String nombre, String autores,
			String categoria) {
		Pageable paging = PageRequest.of(pageNumber, pageSize);
		// LIKE no devuelve resultados con NULL, pero con string vacía funciona como
		// wildcard.
		return bookRepo.findByNombreAutoresCategoriaAndAprobado(nombre == null ? "" : nombre,
				autores == null ? "" : autores, categoria == null ? "" : categoria, paging);
	}

	public Page<Libro> getAllLibrosAprobados(Integer pageNumber, Integer pageSize) {
		Pageable paging = PageRequest.of(pageNumber, pageSize);

		Page<Libro> pagedResult = bookRepo.findAllByAprobado(EstadoLibro.APROBADO, paging);
		return pagedResult;
	}

	public Optional<Libro> getLibroAprobado(Long idLibro) {
		return bookRepo.findByIdAndAprobado(idLibro, EstadoLibro.APROBADO);
	}

	public void deleteBook(Libro book) {
		listRepo.findByIdLibro(book.getId()).parallelStream().forEach(l -> {
			l.getLibros().remove(book);
			logger.debug("Libro borrado: {}", book.getId());
			listRepo.save(l);
		});
		ratingRepo.deleteAll(book.getRatings());
		bookRepo.delete(book);
	}

	public Page<Libro> getLibrosFavoritos(Long idUsuario, Integer pageNumber, Integer pageSize) {
		Pageable paging = PageRequest.of(pageNumber, pageSize);

		Page<Libro> pagedResult = bookRepo.findLibrosFavoritos(idUsuario, paging);
		return pagedResult;
	}
}

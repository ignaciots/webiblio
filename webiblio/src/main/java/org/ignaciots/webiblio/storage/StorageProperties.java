package org.ignaciots.webiblio.storage;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;

@ConfigurationProperties("storage")
@Getter
@Setter
public class StorageProperties {

	/**
	 * Folder location for storing files
	 */
	private String rootLocation = "uploaded";

	private String bookLocation = "uploaded/books";

	private String coverLocation = "uploaded/covers";

}

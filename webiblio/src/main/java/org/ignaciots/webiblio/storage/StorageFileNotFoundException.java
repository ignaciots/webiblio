package org.ignaciots.webiblio.storage;

public class StorageFileNotFoundException extends StorageException {

	private static final long serialVersionUID = -5141725965646124300L;

	public StorageFileNotFoundException(String message) {
		super(message);
	}

	public StorageFileNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}
}

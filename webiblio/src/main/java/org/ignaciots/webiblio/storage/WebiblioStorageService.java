package org.ignaciots.webiblio.storage;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

@Service
/**
 * Servicio de almacenamiento de archivos en el servidor.
 *
 * @author Ignacio Torre Suárez.
 *
 */
public class WebiblioStorageService implements StorageService {

	private final Path rootLocation;
	private final Path bookLocation;
	private final Path coverLocation;
	private Path currentLocation;

	@Autowired
	public WebiblioStorageService(StorageProperties properties) {
		rootLocation = Paths.get(properties.getRootLocation());
		bookLocation = Paths.get(properties.getBookLocation());
		coverLocation = Paths.get(properties.getCoverLocation());
		currentLocation = rootLocation;
	}

	@Override
	public void deleteAll() {
		FileSystemUtils.deleteRecursively(currentLocation.toFile());
	}

	@Override
	public void init() {
		try {
			Files.createDirectories(rootLocation);
			Files.createDirectories(bookLocation);
			Files.createDirectories(coverLocation);
		} catch (IOException e) {
			throw new StorageException("Could not initialize storage", e);
		}
	}

	@Override
	public Path load(String filename) {
		return currentLocation.resolve(filename);
	}

	@Override
	public Stream<Path> loadAll() {
		try {
			return Files.walk(currentLocation, 1).filter(path -> !path.equals(currentLocation))
					.map(currentLocation::relativize);
		} catch (IOException e) {
			throw new StorageException("Failed to read stored files", e);
		}

	}

	@Override
	public Resource loadAsResource(String filename) {
		try {
			Path file = load(filename);
			Resource resource = new UrlResource(file.toUri());
			if (resource.exists() || resource.isReadable()) {
				return resource;
			}
			throw new StorageFileNotFoundException("Could not read file: " + filename);
		} catch (MalformedURLException e) {
			throw new StorageFileNotFoundException("Could not read file: " + filename, e);
		}
	}

	@Override
	public void store(MultipartFile file, Integer nonce) {
		try {
			if (file.isEmpty()) {
				throw new StorageException("Failed to store empty file.");
			}
			Path destinationFile = currentLocation
					.resolve(Paths.get(String.format("%d_%s", nonce, file.getOriginalFilename()))).normalize()
					.toAbsolutePath();
			if (!destinationFile.getParent().equals(currentLocation.toAbsolutePath())) {
				// This is a security check
				throw new StorageException("Cannot store file outside current directory.");
			}
			try (InputStream inputStream = file.getInputStream()) {
				Files.copy(inputStream, destinationFile, StandardCopyOption.REPLACE_EXISTING);
			}
		} catch (IOException e) {
			throw new StorageException("Failed to store file.", e);
		}
	}

	/**
	 * Guarda un archivo en el directorio de libros.
	 *
	 * @param Archivo a guardar.
	 * @param nonce   Nonce para el nombre del archivo.
	 */
	public void storeBook(MultipartFile file, Integer nonce) {
		currentLocation = bookLocation;
		store(file, nonce);
		currentLocation = rootLocation;
	}

	/**
	 * Guarda un archivo en el directorio de portadas.
	 *
	 * @param Archivo a guardar.
	 * @param nonce   Nonce para el nombre del archivo.
	 */
	public void storeCover(MultipartFile file, Integer nonce) {
		currentLocation = coverLocation;
		store(file, nonce);
		currentLocation = rootLocation;
	}
}

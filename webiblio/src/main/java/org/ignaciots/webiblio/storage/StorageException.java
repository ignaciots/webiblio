package org.ignaciots.webiblio.storage;

public class StorageException extends RuntimeException {

	private static final long serialVersionUID = 3583774793631798510L;

	public StorageException(String message) {
		super(message);
	}

	public StorageException(String message, Throwable cause) {
		super(message, cause);
	}
}

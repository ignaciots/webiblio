package org.ignaciots.webiblio.security;

import java.util.ArrayList;
import java.util.List;

import org.ignaciots.webiblio.model.EstadoUsuario;
import org.ignaciots.webiblio.model.Usuario;
import org.ignaciots.webiblio.repository.UsuarioRepository;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
/**
 * Servicio de detalles de usuario autenticado para la aplicación.
 *
 * @author Ignacio Torre Suárez.
 *
 */
public class UsuarioDetailsService implements UserDetailsService {

	private Logger logger = org.slf4j.LoggerFactory.getLogger(UserDetailsService.class);

	@Autowired
	private UsuarioRepository userRepo;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		final Usuario usuario = userRepo.findByNombreUsuario(username).orElseThrow(() -> {
			throw new UsernameNotFoundException(String.format("El usuario %s no existe.", username));
		});

		final Long idUsuario = usuario.getId();
		final String nombre = usuario.getNombreUsuario();
		final String password = usuario.getPassword();
		final boolean habilitado = usuario.getEstado() == EstadoUsuario.HABILITADO;
		final List<GrantedAuthority> authorities = new ArrayList<>();
		authorities.add(new SimpleGrantedAuthority(usuario.getRol().name()));
		logger.info("Usuario cargado: {} {}", nombre, usuario.getRol());
		return new WebiblioUser(idUsuario, nombre, password, habilitado, true, true, true, authorities);
	}

}

package org.ignaciots.webiblio.security;

import javax.annotation.Resource;

import org.ignaciots.webiblio.util.CryptoUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Resource
	private UsuarioDetailsService userDetailsService;

	@Bean
	public DaoAuthenticationProvider authProvider() {
		DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
		authProvider.setUserDetailsService(userDetailsService);
		authProvider.setPasswordEncoder(passwordEncoder());
		return authProvider;
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return CryptoUtil.getBCryptPasswordEncoder();
	}

	@Bean
	@Override
	public UserDetailsService userDetailsService() {
		return new UsuarioDetailsService();
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
				.antMatchers("/", "/resources/**", "/registro**", "/libros", "/libros/**", "/perfil/*",
						"/perfil/*/favoritos", "/login", "/logout", "/listas/*", "/usuarios")
				.permitAll().antMatchers("subida/**").hasAnyAuthority("USUARIO", "MODERADOR", "ADMINISTRADOR")
				.antMatchers("/admin/subidas/**", "admin/usuarios", "admin/usuarios/cambiarestado/**")
				.hasAnyAuthority("MODERADOR", "ADMINISTRADOR")
				.antMatchers("/admin/libros/**", "admin/usuarios/eliminar/**").hasAuthority("ADMINISTRADOR")
				.anyRequest().authenticated().and().formLogin().loginPage("/login").permitAll();
	}
}

package org.ignaciots.webiblio.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import lombok.Getter;
import lombok.Setter;

/**
 * Modelo de usuario autenticado para la aplicación.
 *
 * @author Ignacio Torre Suárez
 *
 */
public class WebiblioUser extends User {

	private static final long serialVersionUID = 5342225891647473025L;

	@Getter
	@Setter
	// Id del usuario
	private Long id;

	public WebiblioUser(Long id, String username, String password, boolean enabled, boolean accountNonExpired,
			boolean credentialsNonExpired, boolean accountNonLocked,
			Collection<? extends GrantedAuthority> authorities) {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
		this.id = id;
	}

}

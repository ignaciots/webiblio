package org.ignaciots.webiblio.forms;

import lombok.Data;

@Data
/**
 * Formulario de búsqueda de libros.
 *
 * @author Ignacio Torre Suárez
 *
 */
public class BusquedaLibroForm {

	private String titulo;

	private String autores;

	private String categoria;
}
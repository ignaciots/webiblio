package org.ignaciots.webiblio.forms;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.web.multipart.MultipartFile;

import lombok.Data;

@Data
/**
 * Formulario de subida de libros.
 *
 * @author Ignacio Torre Suárez
 *
 */
public class SubirLibroForm {

	@Size(max = 255)
	@NotBlank
	private String titulo;

	@Size(max = 255)
	@NotBlank
	private String autores;

	@Size(max = 255)
	@NotBlank
	private String categoria;

	@Size(max = 1000)
	@NotBlank
	private String sinopsis;

	@Min(0)
	@Max(10000)
	@NotNull
	private Integer anyoPublicacion;

	@Min(1)
	@Max(10000)
	@NotNull
	private Integer numeroPaginas;

	@NotNull
	private MultipartFile portada;

	@NotNull
	private MultipartFile libro;

}
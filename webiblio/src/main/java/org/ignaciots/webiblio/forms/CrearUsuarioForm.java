package org.ignaciots.webiblio.forms;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.ignaciots.webiblio.model.EstadoUsuario;
import org.ignaciots.webiblio.model.RolUsuario;

import lombok.Data;

@Data
/**
 * Formulario de creación de usuarios
 *
 * @author Ignacio Torre Suárez
 *
 */
public class CrearUsuarioForm {

	@Size(max = 50)
	@NotBlank
	private String nombreUsuario;

	@Size(max = 512)
	@NotBlank
	private String password;

	@NotNull
	private EstadoUsuario estado;

	@NotNull
	private RolUsuario rol;

}
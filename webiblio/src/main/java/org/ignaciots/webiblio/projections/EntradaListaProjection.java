package org.ignaciots.webiblio.projections;

import org.ignaciots.webiblio.model.EstadoLectura;
import org.ignaciots.webiblio.model.PuntuacionLibro;

/**
 * Proyección para mostrar información de ratings sobre un libro y obtenerla en
 * un repositorio.
 *
 * @author Ignacio Torre Suárez
 *
 */
public interface EntradaListaProjection {

	Long getId();

	String getTitulo();

	EstadoLectura getEstado();

	Boolean getFavorito();

	PuntuacionLibro getPuntuacion();

}

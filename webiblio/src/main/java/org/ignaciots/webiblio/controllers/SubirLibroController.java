package org.ignaciots.webiblio.controllers;

import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Set;

import javax.validation.Valid;

import org.ignaciots.webiblio.forms.SubirLibroForm;
import org.ignaciots.webiblio.model.EstadoLibro;
import org.ignaciots.webiblio.model.Libro;
import org.ignaciots.webiblio.model.RolUsuario;
import org.ignaciots.webiblio.repository.CategoriaRepository;
import org.ignaciots.webiblio.repository.LibroRepository;
import org.ignaciots.webiblio.services.UsuarioService;
import org.ignaciots.webiblio.storage.WebiblioStorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.server.ResponseStatusException;

@Controller
@RequestMapping("/subida")
/**
 * Controlador de subidas de libro.
 *
 * @author Ignacio Torre Suárez.
 *
 */
public class SubirLibroController {

	private static final int MAX_BOOK_SIZE = 20971520; // 20 MiB

	private static final String[] EXTENSIONES_LIBRO = { ".pdf", ".epub", ".txt", ".html" };
	private static final String[] EXTENSIONES_IMAGEN = { ".png", ".jpg", ".jpeg", ".gif" };

	private final WebiblioStorageService storageService;

	@Autowired
	private LibroRepository bookRepo;

	@Autowired
	private CategoriaRepository categoryRepo;

	@Autowired
	private UsuarioService userService;

	private Logger logger = LoggerFactory.getLogger(SubirLibroController.class);

	@Autowired
	public SubirLibroController(WebiblioStorageService storageService) {
		this.storageService = storageService;
	}

	@GetMapping("")
	/**
	 * Muestra el formulario de subida de libro.
	 *
	 * @param model          Modelo de la vista.
	 * @param subirLibroForm Formulario de subida.
	 * @return Vista de subida.
	 */
	public String formularioSubirLibro(Model model, SubirLibroForm subirLibroForm) {
		logger.info("Mostrando formulario de subida de libro");
		model.addAttribute("categorias", categoryRepo.findAll());
		return "libros/subir";
	}

	@PostMapping("")
	public String formularioSubirLibro(Model model, @Valid SubirLibroForm subirLibroForm, BindingResult result) {
		// Errores
		logger.info("Intentando subir libro {} - {}", subirLibroForm.getLibro(), subirLibroForm.getPortada());
		if (subirLibroForm.getLibro().isEmpty()) {
			logger.warn("El libro subido está vacío.");
			result.addError(new FieldError("subirLibroForm", "libro", "Este campo es obligatorio."));
		}
		if (subirLibroForm.getPortada().isEmpty()) {
			logger.warn("La portada subida está vacía.");
			result.addError(new FieldError("subirLibroForm", "portada", "Este campo es obligatorio."));
		}

		// Extensión de archivo
		if (!esTipoArchivoLibro(subirLibroForm.getLibro().getOriginalFilename())) {
			logger.warn("Extensión incorrecta: {}", subirLibroForm.getLibro().getOriginalFilename());
			result.addError(new FieldError("subirLibroForm", "libro",
					"Extensión no válida. Las extensiones admitidas son: pdf, epub, txt, html."));
		}
		if (!esTipoArchivoImagen(subirLibroForm.getPortada().getOriginalFilename())) {
			logger.warn("Extensión incorrecta: {}", subirLibroForm.getPortada().getOriginalFilename());
			result.addError(new FieldError("subirLibroForm", "portada",
					"Extensión no válida. Las extensiones admitidas son: png, jpg, jpeg, gif."));
		}

		long sizeTotalArchivos = subirLibroForm.getLibro().getSize() + subirLibroForm.getPortada().getSize();
		if (sizeTotalArchivos > MAX_BOOK_SIZE * 2) {
			logger.warn("El tamaño de los archivos es demasiado grande: {}", sizeTotalArchivos);
			result.addError(new FieldError("subirLibroForm", "libro",
					"El tamaño de los archivos es demasiado grande. El tamaño conjunto de ambos archivos no debe superar los 40 MiB"));
		} else {
			if (subirLibroForm.getLibro().getSize() > MAX_BOOK_SIZE) {
				logger.warn("El tamaño del libro es demasiado grande: {}", subirLibroForm.getLibro().getSize());
				result.addError(new FieldError("subirLibroForm", "libro",
						"El libro es demasiado grande. El tamaño del archivo no debe superar los 20 MiB."));
			}
			if (subirLibroForm.getPortada().getSize() > MAX_BOOK_SIZE) {
				logger.warn("El tamaño de la portada es demasiado grande: {}", subirLibroForm.getPortada().getSize());
				result.addError(new FieldError("subirLibroForm", "portada",
						"La portada es demasiado grande. El tamaño del archivo no debe superar los 20 MiB."));
			}
		}

		if (result.hasErrors()) {
			logger.warn("Errores en el formulario");
			model.addAttribute("categorias", categoryRepo.findAll());
			return "libros/subir";
		}

		// Comprobar la categoría
		var categoriaLibro = categoryRepo.findByNombre(subirLibroForm.getCategoria())
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Categoría no válida."));

		// Crear el libro
		Libro libro = new Libro();
		libro.setTitulo(subirLibroForm.getTitulo());
		libro.setAutores(subirLibroForm.getAutores());
		libro.setCategorias(Set.of(categoriaLibro));
		libro.setSinopsis(subirLibroForm.getSinopsis());
		libro.setAnyoPublicacion(subirLibroForm.getAnyoPublicacion());
		libro.setNumeroPaginas(subirLibroForm.getNumeroPaginas());

		// Establecer estado según usuario actual
		userService.getUsuarioActual().ifPresentOrElse(u -> {
			if (u.getRol() == RolUsuario.USUARIO) {
				libro.setAprobado(EstadoLibro.POR_APROBAR);
			} else {
				libro.setAprobado(EstadoLibro.APROBADO);
			}
		}, () -> {
			libro.setAprobado(EstadoLibro.POR_APROBAR);
		});

		String estadoLibroAprobado = "poraprobar";
		if (libro.getAprobado() == EstadoLibro.APROBADO) {
			estadoLibroAprobado = "aprobado";
		}

		logger.info("Libro en estado {}", libro.getAprobado());

		Integer nonce = bookRepo.findHibernateNextVal();
		storageService.storeBook(subirLibroForm.getLibro(), nonce);
		storageService.storeCover(subirLibroForm.getPortada(), nonce);
		libro.setPathLibro(String.format("/resources/books/%d_%s", nonce,
				Paths.get(subirLibroForm.getLibro().getOriginalFilename()).normalize()));
		libro.setPathImagen(String.format("/resources/covers/%d_%s", nonce,
				Paths.get(subirLibroForm.getPortada().getOriginalFilename()).normalize()));

		bookRepo.save(libro);
		logger.info("Libro creado correctamente");
		return "redirect:/subida?" + estadoLibroAprobado;
	}

	private boolean esTipoArchivoLibro(String name) {
		return Arrays.stream(EXTENSIONES_LIBRO).anyMatch(e -> name.endsWith(e));
	}

	private boolean esTipoArchivoImagen(String name) {
		return Arrays.stream(EXTENSIONES_IMAGEN).anyMatch(e -> name.endsWith(e));
	}
}

package org.ignaciots.webiblio.controllers;

import org.ignaciots.webiblio.repository.ListaRepository;
import org.ignaciots.webiblio.services.LibroService;
import org.ignaciots.webiblio.services.ListaService;
import org.ignaciots.webiblio.services.RatingService;
import org.ignaciots.webiblio.services.UsuarioService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.server.ResponseStatusException;

@Controller
@RequestMapping("/listas")
/**
 * Controlador que maneja las listas de usuario.
 *
 * @author Ignacio Torre Suárez
 *
 */
public class ListaController {

	@Autowired
	private LibroService bookService;

	@Autowired
	private ListaRepository listRepo;

	@Autowired
	private ListaService listService;

	@Autowired
	private RatingService ratingsService;

	@Autowired
	private UsuarioService userService;

	private Logger logger = LoggerFactory.getLogger(ListaController.class);

	@GetMapping("{id}")
	/**
	 * Muestra la lista de un usuario
	 *
	 * @param model   Model de la vista.
	 * @param idLista Id de la lista.
	 * @return Vista de la lista.
	 */
	public String verLista(Model model, @PathVariable("id") Long idLista,
			@RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "10") Integer size) {

		var lista = listRepo.findById(idLista)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Lista no encontrada."));
		if (listService.perteneceUsuarioActual(idLista)) {
			model.addAttribute("mismoUsuario", true);
			logger.info("Accediendo a lista {} por el mismo usuario.", idLista);
		} else {
			logger.info("Accediendo a lista {}.", idLista);
		}

		if (!listService.puedeAccederLista(idLista)) {
			logger.info("La lista {} es privada.", idLista);
			model.addAttribute("privado", true);
		} else {
			logger.info("La lista {} NO es privada.", idLista);
			model.addAttribute("lista", lista);
			var ratings = ratingsService.getRatingsByLista(idLista, page, size);
			model.addAttribute("ratings", ratings);
		}
		return "listas/lista";
	}

	@PostMapping("eliminar/{id}")
	/**
	 * Elimina una lista y redirige al perfil de usuario.
	 *
	 * @param idLista Id de la lista a eliminar.
	 * @return Redirección al perfil de usuario.
	 */
	public String eliminarLista(@PathVariable("id") Long idLista) {
		logger.info("Intentado eliminar lista {}", idLista);
		if (!listService.perteneceUsuarioActual(idLista)) {
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, "No tienes permisos para realizar esta acción.");
		}
		var lista = listRepo.findById(idLista)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Lista no encontrada."));
		listRepo.delete(lista);

		logger.info("Lista eliminada: {}", idLista);
		return "redirect:/perfil/" + userService.getUsuarioActual().get().getId() + "?listaEliminada=1";
	}

	@GetMapping(value = "add/{idLibro}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	/**
	 * Añade un libro a la lista.
	 *
	 * @param idLista Id de la lista.
	 * @param idLibro Id del libro a añadir.
	 * @return True si se ha añadido, false en otro caso.
	 */
	public boolean addLibroLista(@RequestParam(name = "idLista", required = true) Long idLista,
			@PathVariable("idLibro") Long idLibro) {
		logger.info("Intentado añadir libro {} a lista {}", idLibro, idLista);
		if (!listService.perteneceUsuarioActual(idLista)) {
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, "No tienes permisos para realizar esta acción.");
		}
		var lista = listRepo.findById(idLista)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Lista no encontrada."));
		var libro = bookService.getLibroAprobado(idLibro)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Libro no encontrado."));
		if (!lista.getLibros().contains(libro)) {
			lista.getLibros().add(libro);
			listRepo.save(lista);
			logger.info("Libro añadido a lista");
			return true;
		}
		logger.info("El libro ya se encontraba en la lista");
		return false;
	}

	@PostMapping("{idLista}/libro/eliminar/{idLibro}")
	/**
	 * Elimina un libro de la lista.
	 *
	 * @param idLista ID de la lista.
	 * @param idLibro Id del libro.
	 * @return Redirección a la lista del usuario.
	 */
	public String eliminarLibroLista(@PathVariable("idLista") Long idLista, @PathVariable("idLibro") Long idLibro) {
		logger.info("Intentando eliminar libro {} de la lista {}", idLibro, idLista);
		if (!listService.perteneceUsuarioActual(idLista)) {
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, "No tienes permisos para realizar esta acción.");
		}
		var lista = listRepo.findById(idLista)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Lista no encontrada."));
		lista.getLibros().removeIf(l -> l.getId() == idLibro);
		listRepo.save(lista);

		return "redirect:/listas/" + idLista + "?eliminarLibro=1";
	}
}

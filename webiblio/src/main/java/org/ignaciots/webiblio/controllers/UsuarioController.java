package org.ignaciots.webiblio.controllers;

import org.ignaciots.webiblio.model.Usuario;
import org.ignaciots.webiblio.services.UsuarioService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/usuarios")
/**
 * Controlador relativo a usuarios.
 *
 * @author Ignacio Torre Suárez
 *
 */
public class UsuarioController {

	@Autowired
	private UsuarioService userService;

	private Logger logger = LoggerFactory.getLogger(UsuarioController.class);

	@GetMapping("")
	/**
	 * Muestra el listado de usuarios públicos.
	 *
	 * @param model         Modelo de la vista.
	 * @param page          Página de la paginación.
	 * @param size          Tamaño de la página de paginación.
	 * @param nombreUsuario El nombre de usuario de la búsqueda.
	 * @return La vista de listado de usuarios públicos.
	 */
	public String verUsuarios(Model model, @RequestParam(defaultValue = "0") Integer page,
			@RequestParam(defaultValue = "10") Integer size,
			@RequestParam(name = "nombre", defaultValue = "") String nombreUsuario) {

		Page<Usuario> usuarios;
		// Otra opción sería comprobar en la plantilla que la variable no sea nula.
		String ultimaBusqueda = "";
		if (nombreUsuario != null && !nombreUsuario.isBlank()) {
			ultimaBusqueda += "&nombre=" + nombreUsuario;
		}
		logger.info("Accediendo a vista de usuarios públicos con búsqueda: {}", ultimaBusqueda);

		usuarios = userService.buscarUsuariosPublicos(nombreUsuario, page, size);
		model.addAttribute("ultimaBusqueda", ultimaBusqueda);
		model.addAttribute("usuarios", usuarios);
		return "usuarios/listado";
	}
}

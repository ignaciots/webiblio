package org.ignaciots.webiblio.controllers;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
/**
 * Controlador para mostrar errores.
 *
 * @author Ignacio Torre Suárez
 *
 */
public class WebiblioErrorController implements ErrorController {

	@RequestMapping("/error")
	/**
	 * Muestra errores de la página web.
	 *
	 * @param model Modelo de la vista.
	 * @param req   {@link HttpServletRequest}
	 * @return Vista de error.
	 */
	public String showError(Model model, HttpServletRequest req) {
		Integer statusCode = (Integer) req.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
		String reason = HttpStatus.valueOf(statusCode).getReasonPhrase();
		model.addAttribute("statusCode", statusCode);
		model.addAttribute("reason", reason);
		return "error";
	}

	@Override
	public String getErrorPath() {
		// dejar en null
		return null;
	}
}

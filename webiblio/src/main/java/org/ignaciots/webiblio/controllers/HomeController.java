package org.ignaciots.webiblio.controllers;

import java.util.List;

import org.ignaciots.webiblio.model.Libro;
import org.ignaciots.webiblio.repository.LibroRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
/**
 * Controlador para la página principal.
 *
 * @author Ignacio Torre Suárez.
 *
 */
public class HomeController {

	private static final int NUM_POPULARES_POR_SECCION = 3;
	private final Logger logger = LoggerFactory.getLogger(HomeController.class);

	@Autowired
	private LibroRepository bookRepo;

	@GetMapping("")
	public String index(Model model) {
		logger.info("Accediendo al índice");
		// Dividir libros populares en dos listas de como máximo 3 elementos cada una
		List<Libro> librosPopulares = bookRepo.findAllOrderedByPopular();
		int sizeLibrosPopulares = librosPopulares.size();
		if (sizeLibrosPopulares > 0) {
			if (sizeLibrosPopulares < NUM_POPULARES_POR_SECCION - 1) {
				logger.info("Creando página 1 de libros populares.");
				model.addAttribute("librosPopulares1", librosPopulares.subList(0, sizeLibrosPopulares));
			} else {
				logger.info("Creando páginas 1 y 2 de libros populares.");
				model.addAttribute("librosPopulares1", librosPopulares.subList(0, NUM_POPULARES_POR_SECCION));
				model.addAttribute("librosPopulares2",
						librosPopulares.subList(NUM_POPULARES_POR_SECCION, sizeLibrosPopulares));
			}
		}

		return "index";
	}
}

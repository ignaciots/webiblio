package org.ignaciots.webiblio.controllers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.ignaciots.webiblio.model.Lista;
import org.ignaciots.webiblio.model.NivelPrivacidad;
import org.ignaciots.webiblio.repository.ListaRepository;
import org.ignaciots.webiblio.services.LibroService;
import org.ignaciots.webiblio.services.UsuarioService;
import org.ignaciots.webiblio.util.AuthUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.server.ResponseStatusException;

@Controller
@RequestMapping("/perfil")
/**
 * Controlador con acciones relativas al perfil.
 *
 * @author Ignacio Torre Suárez.
 *
 */
public class PerfilController {

	@Autowired
	private UsuarioService userService;

	@Autowired
	private ListaRepository listRepo;

	@Autowired
	private LibroService bookService;

	private Logger logger = LoggerFactory.getLogger(PerfilController.class);

	@GetMapping(value = "privacidad/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	/**
	 * Actualiza el nivel de privacidad del usuario.
	 *
	 * @param idUsuario       El Id del usuario.
	 * @param nivelPrivacidad El nuevo nivel de privacidad
	 * @return true.
	 */
	public boolean actualizarNivelPrivacidad(@PathVariable("id") Long idUsuario, @RequestParam String nivelPrivacidad) {
		logger.info("Intentando actualizar el nivel de privacidad del usuario {} a {}", idUsuario, nivelPrivacidad);
		var usuario = userService.getUsuarioSiActivo(idUsuario)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Usuario no encontrado."));

		if (!AuthUtil.esMismoUsuario(usuario.getNombreUsuario())) {
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, "No tienes permisos para realizar esta acción.");
		}

		userService.cambiarNivelPrivacidad(usuario, nivelPrivacidad);
		logger.info("Nivel de privacidad cambiado: {} a {}", idUsuario, nivelPrivacidad);
		return true;
	}

	@PostMapping("{id}/lista")
	/**
	 * Crea una nueva lista y redirige al perfil de usuario.
	 *
	 * @param idUsuario   Id de usuario.
	 * @param nombreLista Id de la nueva lista.
	 * @return Redirección del perfil de usuario.
	 */
	public String crearNuevaLista(@PathVariable("id") Long idUsuario, @RequestParam String nombreLista) {
		logger.info("Intentando crear nueva lista para usuario {} con nombre {}", idUsuario, nombreLista);
		var usuario = userService.getUsuarioSiActivo(idUsuario)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Usuario no encontrado."));

		if (!AuthUtil.esMismoUsuario(usuario.getNombreUsuario())) {
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, "No tienes permisos para realizar esta acción.");
		}

		if (nombreLista.isBlank()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Nombre de lista no válido.");
		}

		var lista = new Lista();
		lista.setUsuario(usuario);
		lista.setNombre(nombreLista);
		listRepo.save(lista);
		logger.info("Creada lista {}", nombreLista);

		return "redirect:/perfil/" + idUsuario + "?success=1";
	}

	@GetMapping("{id}")
	/**
	 * Muestra el perfil de un usuario.
	 *
	 * @param model     Modelo de la lista
	 * @param idUsuario Id del usuario.
	 * @return Vista del perfil.
	 */
	public String verPerfil(Model model, @PathVariable("id") Long idUsuario,
			@RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "10") Integer size) {
		logger.info("Accediendo al perfil de {}", idUsuario);
		var usuario = userService.getUsuarioSiActivo(idUsuario)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Usuario no encontrado."));
		if (!userService.puedeAccederPerfil(usuario.getNombreUsuario(), usuario.getNivelPrivacidad())) {
			logger.info("El perfil de {} es privado", idUsuario);
			model.addAttribute("privado", true);
			return "usuarios/perfil";
		}
		if (AuthUtil.esMismoUsuario(usuario.getNombreUsuario())) {
			logger.info("El usuario accede a su propio perfil.");
			model.addAttribute("mismoUsuario", true);
			model.addAttribute("nivelPrivacidadActual", usuario.getNivelPrivacidad());
		}
		model.addAttribute("listas", paginarListas(usuario.getListas(), PageRequest.of(page, size)));
		model.addAttribute("nombre", usuario.getNombreUsuario());
		model.addAttribute("nivelesPrivacidad", NivelPrivacidad.values());
		;
		return "usuarios/perfil";
	}

	@GetMapping("{id}/favoritos")
	/**
	 * Muestra el listado de favoritos de un usuario.
	 *
	 * @param model     Modelo de la vista.
	 * @param page      Página de la paginación.
	 * @param size      Tamaño de la página de paginación.
	 * @param idUsuario Id del usuario.
	 * @return La vista de listado de favoritos.
	 */
	public String verFavoritosUsuario(Model model, @RequestParam(defaultValue = "0") Integer page,
			@RequestParam(defaultValue = "5") Integer size, @PathVariable("id") Long idUsuario) {
		logger.info("Intentando acceder a los favoritos de {}", idUsuario);
		var usuario = userService.getUsuarioSiActivo(idUsuario)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Usuario no encontrado."));
		model.addAttribute("nombreUsuario", usuario.getNombreUsuario());
		if (!userService.puedeAccederPerfil(usuario.getNombreUsuario(), usuario.getNivelPrivacidad())) {
			model.addAttribute("privado", true);
			logger.info("El perfil de {} es privado", idUsuario);
			return "usuarios/perfil";
		}
		if (AuthUtil.esMismoUsuario(usuario.getNombreUsuario())) {
			logger.info("El usuario accede a su mismo perfil.");
			model.addAttribute("mismoUsuario", true);
			model.addAttribute("nivelPrivacidadActual", usuario.getNivelPrivacidad());
		}
		model.addAttribute("libros", bookService.getLibrosFavoritos(idUsuario, page, size));
		;
		return "usuarios/favoritos";
	}

	private Page<Lista> paginarListas(final Set<Lista> listasPaginables, Pageable pageable) {
		int pageSize = pageable.getPageSize();
		int currentPage = pageable.getPageNumber();
		int startItem = currentPage * pageSize;
		List<Lista> listaActual;
		List<Lista> listaOriginal = new ArrayList<>(listasPaginables);

		if (listaOriginal.size() < startItem) {
			listaActual = Collections.emptyList();
		} else {
			int toIndex = Math.min(startItem + pageSize, listaOriginal.size());
			listaActual = listaOriginal.subList(startItem, toIndex);
		}

		Page<Lista> bookPage = new PageImpl<>(listaActual, PageRequest.of(currentPage, pageSize), listaOriginal.size());

		return bookPage;
	}

}

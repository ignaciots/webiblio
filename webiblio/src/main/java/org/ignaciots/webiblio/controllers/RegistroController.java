package org.ignaciots.webiblio.controllers;

import javax.validation.Valid;

import org.ignaciots.webiblio.exceptions.UsuarioYaExisteException;
import org.ignaciots.webiblio.model.Usuario;
import org.ignaciots.webiblio.services.UsuarioService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
/**
 * Controlador de registro de usuario.
 *
 * @author Ignacio Torre Suárez.
 *
 */
public class RegistroController {

	@Autowired
	private UsuarioService userService;

	private Logger logger = LoggerFactory.getLogger(RegistroController.class);

	@GetMapping("/registro")
	/**
	 * Muestra el formulario de registro.
	 *
	 * @param model Modelo de la vista.
	 * @return Vista de registro.
	 */
	public String mostrarFormRegistro(Model model) {
		logger.info("Mostrando registro");
		Usuario usuario = new Usuario();
		model.addAttribute("usuario", usuario);
		return "auth/registro";
	}

	@PostMapping("/registro")
	/**
	 * Registra a un usuario
	 *
	 * @param usuario Usuario a registrar.
	 * @param results Resultados del formulario.
	 * @return Vista de registro.
	 */
	public ModelAndView registerUserAccount(@ModelAttribute("usuario") @Valid Usuario usuario, BindingResult results) {
		logger.info("Intentando registrar usuario {}", usuario.getNombreUsuario());
		var modelView = new ModelAndView("redirect:/login?registrado=1", "usuario", usuario);
		if (results.hasErrors()) {
			logger.warn("Errores en el formulario de registro");
			modelView.setViewName("auth/registro");
			return modelView;
		}
		try {
			userService.registrarUsuario(usuario);
			logger.info("Usuario {} registrado", usuario.getNombreUsuario());
		} catch (UsuarioYaExisteException e) {
			logger.info("El usuario a registrar ya existe");
			modelView.addObject("userAlreadyExists", "El usuario ya existe.");
			modelView.setViewName("auth/registro");
		}
		return modelView;
	}

}

package org.ignaciots.webiblio.controllers;

import org.ignaciots.webiblio.forms.BusquedaLibroForm;
import org.ignaciots.webiblio.model.EstadoLectura;
import org.ignaciots.webiblio.model.Libro;
import org.ignaciots.webiblio.model.PuntuacionLibro;
import org.ignaciots.webiblio.repository.CategoriaRepository;
import org.ignaciots.webiblio.services.LibroService;
import org.ignaciots.webiblio.services.ListaService;
import org.ignaciots.webiblio.services.RatingService;
import org.ignaciots.webiblio.services.UsuarioService;
import org.ignaciots.webiblio.util.AuthUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.server.ResponseStatusException;

@Controller
@RequestMapping("/libros")
/**
 * Controlador sobre operaciones del listado de libros.
 *
 * @author Ignacio Torre Suárez.
 *
 */
public class ListadoController {

	@Autowired
	private LibroService bookService;

	@Autowired
	private ListaService listService;

	@Autowired
	private CategoriaRepository categoriaRepo;

	@Autowired
	private RatingService ratingService;

	@Autowired
	private UsuarioService userService;

	private Logger logger = LoggerFactory.getLogger(ListaController.class);

	@GetMapping("{id}")
	/**
	 * Muestra los detalles de un libro.
	 *
	 * @param model   Model de la vista.
	 * @param idLibro Id del libro.
	 * @return Vista de detalles de libro.
	 */
	public String detallesLibro(Model model, @PathVariable("id") Long idLibro) {
		logger.info("Intentando acceder a detalles del libro {}", idLibro);
		Libro libro = bookService.getLibroAprobado(idLibro)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Libro no encontrado."));
		model.addAttribute("libro", libro);
		model.addAttribute("puntuaciones", PuntuacionLibro.values());
		model.addAttribute("estadosLibro", EstadoLectura.values());

		if (AuthUtil.estaUsuarioAutenticado()) {
			logger.info("Mostrando formularios de usuario autenticado");
			var usuarioActual = userService.getUsuarioActual().get();
			var listasUsuarioActual = listService.getListasUsuario();
			model.addAttribute("listasUsuario", listasUsuarioActual);
			model.addAttribute("favorito", ratingService.esFavorito(usuarioActual, libro));
			model.addAttribute("puntuacionUsuario",
					ratingService.getPuntuacion(usuarioActual, libro).orElse(PuntuacionLibro.UNA_ESTRELLA));
			model.addAttribute("estadoLecturaUsuario",
					ratingService.getEstadoLectura(usuarioActual, libro).orElse(EstadoLectura.PLAN_LEER));
		}
		return "libros/detalles";
	}

	@GetMapping("")
	/**
	 * Muestra el listado de libros.
	 *
	 * @param model             Modelo de la vista.
	 * @param busquedaLibroForm Formulario de búsqueda de libro.
	 * @param page              Página de la paginación.
	 * @param size              Tamaño de la página de paginación.
	 * @return La vista de listado de libros.
	 */
	public String listado(Model model, @RequestParam(defaultValue = "0") Integer page,
			@RequestParam(defaultValue = "5") Integer size, BusquedaLibroForm busquedaLibroForm) {

		Page<Libro> libros;
		// Otra opción sería comprobar en la plantilla que la variable no sea nula.
		String ultimaBusqueda = "";
		if (busquedaLibroForm.getTitulo() != null && !busquedaLibroForm.getTitulo().isBlank()) {
			ultimaBusqueda += "&titulo=" + busquedaLibroForm.getTitulo();
		}
		if (busquedaLibroForm.getAutores() != null && !busquedaLibroForm.getAutores().isBlank()) {
			ultimaBusqueda += "&autores=" + busquedaLibroForm.getAutores();
		}
		if (busquedaLibroForm.getCategoria() != null && !busquedaLibroForm.getCategoria().isBlank()) {
			ultimaBusqueda += "&categoria=" + busquedaLibroForm.getCategoria();
		}
		logger.info("Listado libros con filtro de búsqueda: {}", ultimaBusqueda);

		libros = bookService.buscarLibrosAprobados(page, size, busquedaLibroForm.getTitulo(),
				busquedaLibroForm.getAutores(), busquedaLibroForm.getCategoria());
		model.addAttribute("ultimaBusqueda", ultimaBusqueda);

		model.addAttribute("categorias", categoriaRepo.findAll());
		model.addAttribute("libros", libros);
		return "libros/listado";
	}
}

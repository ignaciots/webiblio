package org.ignaciots.webiblio.controllers;

import javax.validation.Valid;

import org.ignaciots.webiblio.exceptions.UsuarioYaExisteException;
import org.ignaciots.webiblio.forms.CrearUsuarioForm;
import org.ignaciots.webiblio.model.EstadoLibro;
import org.ignaciots.webiblio.model.EstadoUsuario;
import org.ignaciots.webiblio.model.RolUsuario;
import org.ignaciots.webiblio.model.Usuario;
import org.ignaciots.webiblio.repository.LibroRepository;
import org.ignaciots.webiblio.repository.UsuarioRepository;
import org.ignaciots.webiblio.services.LibroService;
import org.ignaciots.webiblio.services.UsuarioService;
import org.ignaciots.webiblio.util.AuthUtil;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.server.ResponseStatusException;

@Controller
@RequestMapping("/admin")
/**
 * Controlador que se encarga de las tareas relacionadas con la administración.
 *
 * @author Ignacio Torre Suárez
 *
 */
public class AdminController {

	private static final int SIZE_PAGINA_PREDETERMINADO = 5;

	private static final int PAGINA_PREDETERMINADA = 0;

	@Autowired
	private UsuarioService userService;

	@Autowired
	private UsuarioRepository userRepo;

	@Autowired
	private LibroService bookService;

	@Autowired
	private LibroRepository bookRepo;

	private final Logger logger = org.slf4j.LoggerFactory.getLogger(AdminController.class);

	@GetMapping("usuarios")
	/**
	 * Muestra el panel de usuarios.
	 *
	 * @param model            Modelo de la vista.
	 * @param crearUsuarioForm Formulario de creación de usuario.
	 * @param page             Página de la paginación.
	 * @param size             Tamaño de la página de paginación.
	 * @return La vista de listado de usuarios.
	 */
	public String verPanelUsuarios(Model model, CrearUsuarioForm crearUsuarioForm,
			@RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "10") Integer size) {
		logger.info("Accediendo al panel de listado de usuarios para administradores");
		setUpPanelUsuarios(model, page, size);
		return "admin/usuarios";
	}

	private void setUpPanelUsuarios(Model model, Integer page, Integer size) {
		var rol = userService.getUsuarioActual().get().getRol();
		logger.info("Rol del usuario que accede: {}", rol);
		// Obtener usuarios que se pueden borrar dependiendo del rol del usuario.
		Page<Usuario> usuarios;
		if (rol == RolUsuario.ADMINISTRADOR) {
			usuarios = userService.getUsuarios(page, size);
		} else if (rol == RolUsuario.MODERADOR) {
			usuarios = userService.getUsuariosRasos(page, size);
		} else {
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Esta ruta no es accesible para tu rol.");
		}

		// Lista de estados para el selector.
		model.addAttribute("estadosUsuario", EstadoUsuario.values());
		// Lista de roles para el selector.
		model.addAttribute("rolesUsuario", RolUsuario.values());
		model.addAttribute("usuarios", usuarios);
	}

	@PostMapping("usuarios")
	/**
	 * Crea un usuario y redirige a la lista de usuarios.
	 *
	 * @param model            Modelo de la vista.
	 * @param crearUsuarioForm Formulario de creación de usuario.
	 * @param result           Resultado de la validación del formulario.
	 * @return La vista de listado de usuarios.
	 */
	public String postCrearUsuario(Model model, @Valid CrearUsuarioForm crearUsuarioForm, BindingResult result) {
		if (result.hasErrors()) {
			logger.warn("Creación de usuario ha fallado con errores.");
			// Valores predeterminados
			setUpPanelUsuarios(model, PAGINA_PREDETERMINADA, SIZE_PAGINA_PREDETERMINADO);
			return "admin/usuarios";
		}

		var usuario = new Usuario();
		usuario.setNombreUsuario(crearUsuarioForm.getNombreUsuario());
		usuario.setPassword(crearUsuarioForm.getPassword());
		usuario.setEstado(crearUsuarioForm.getEstado());
		usuario.setRol(crearUsuarioForm.getRol());
		try {
			userService.crearUsuario(usuario);
			logger.info("Creación del usuario: {}", usuario.getNombreUsuario());
		} catch (UsuarioYaExisteException e) {
			logger.info("Creación del usuario fallida, ya existe: {}", usuario.getNombreUsuario());
			result.addError(new FieldError("crearUsuarioForm", "nombreUsuario", "El usuario ya existe."));
			setUpPanelUsuarios(model, PAGINA_PREDETERMINADA, SIZE_PAGINA_PREDETERMINADO);
			return "admin/usuarios";
		}
		return "redirect:/admin/usuarios?success=1";
	}

	@PostMapping("usuarios/cambiarestado/{id}")
	/**
	 * Deshabilita un usuario y redirige a la lista de usuarios.
	 *
	 * @param idUsuario ID de usuario a deshabilitar.
	 * @return Redirección a la vista de listado de usuarios.
	 */
	public String deshabilitarUsuario(@PathVariable("id") Long idUsuario) {
		logger.info("Intentando cambiar estado de usuario: {}", idUsuario);
		var user = userRepo.findById(idUsuario).orElseThrow(() -> {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Usuario no encontrado.");
		});

		if (!tienePermisosDeshabilitar(user)) {
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, "No tienes permisos para realizar esta acción.");
		}
		user.setEstado(
				user.getEstado() == EstadoUsuario.HABILITADO ? EstadoUsuario.DESHABILITADO : EstadoUsuario.HABILITADO);
		userRepo.save(user);
		logger.info("Estado cambiado: {}", user.getEstado());

		// Redirigir y activar mensaje de éxito.
		return "redirect:/admin/usuarios?estadoCambiado=1";
	}

	private boolean tienePermisosDeshabilitar(Usuario user) {
		// Comprobar permisos para deshabilitar un usuario: solo mods y admin pueden
		// deshabilitar usuarios; los mods solo pueden deshabilitar usuarios rasos.
		var usuarioActual = userService.getUsuarioActual().get();
		if (usuarioActual.getRol() == RolUsuario.MODERADOR) {
			return user.getRol() == RolUsuario.USUARIO;
		}
		return usuarioActual.getRol() == RolUsuario.ADMINISTRADOR;
	}

	@PostMapping("usuarios/eliminar/{id}")
	/**
	 * Elimina un usuario y redirige a la lista de usuarios.
	 *
	 * @param idUsuario ID de usuario a deshabilitar.
	 * @return Redirección a la vista de listado de usuarios.
	 */
	public String eliminarUsuario(@PathVariable("id") Long idUsuario) {
		logger.info("Intentando eliminar usuario {}", idUsuario);
		var user = userRepo.findById(idUsuario).orElseThrow(() -> {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Usuario no encontrado.");
		});

		var usuarioActual = userService.getUsuarioActual().orElseThrow(() -> {
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Esta ruta no es accesible para tu rol.");
		});

		if (user.getNombreUsuario().equals(usuarioActual.getNombreUsuario())) {
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, "No puedes eliminar a este usuario.");
		}

		userService.deleteUser(user);
		logger.info("Usuario eliminado: {}", idUsuario);

		return "redirect:/admin/usuarios?usuarioEliminado=1";
	}

	@GetMapping("libros")
	/**
	 * Muestra el panel de libros.
	 *
	 * @param model            Modelo de la vista.
	 * @param crearUsuarioForm Formulario de creación de usuario.
	 * @param page             Página de la paginación.
	 * @param size             Tamaño de la página de paginación.
	 * @return La vista de listado de libros.
	 */
	public String verPanelLibros(Model model, @RequestParam(defaultValue = "0") Integer page,
			@RequestParam(defaultValue = "10") Integer size) {
		logger.info("Accediendo al panel de libros");
		if (!AuthUtil.estaUsuarioAutenticado()) {
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Esta ruta no es accesible para tu rol.");
		}
		var rol = userService.getUsuarioActual().get().getRol();

		if (rol == RolUsuario.USUARIO) {
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Esta ruta no es accesible para tu rol.");
		}

		Pageable pageable = PageRequest.of(page, size);
		model.addAttribute("libros", bookRepo.findAll(pageable));
		return "admin/libros";
	}

	@PostMapping("libros/eliminar/{id}")
	/**
	 * Elimina un libro y redirige al panel de libros.
	 *
	 * @param idLibro Id del libro a eliminar.
	 * @return Redirección al panel de libros.
	 */
	public String eliminarLibro(@PathVariable("id") Long idLibro) {
		logger.info("Intentando eliminar libro {}", idLibro);
		var book = bookRepo.findById(idLibro).orElseThrow(() -> {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Usuario no encontrado.");
		});

		bookService.deleteBook(book);
		logger.info("Libro eliminado {}", idLibro);
		return "redirect:/admin/libros?success=1";
	}

	@GetMapping("subidas")
	/**
	 * Muestra el panel de subidas.
	 *
	 * @param model            Modelo de la vista.
	 * @param crearUsuarioForm Formulario de creación de usuario.
	 * @param page             Página de la paginación.
	 * @param size             Tamaño de la página de paginación.
	 * @return La vista de listado de subidas.
	 */
	public String verPanelSubidas(Model model, @RequestParam(defaultValue = "0") Integer page,
			@RequestParam(defaultValue = "10") Integer size) {
		logger.info("Accediendo a la lista de subidas");
		Pageable pageable = PageRequest.of(page, size);
		model.addAttribute("libros", bookRepo.findByAprobado(EstadoLibro.POR_APROBAR, pageable));
		return "admin/subidas";
	}

	@PostMapping("subidas/aprobar/{id}")
	/**
	 * Aprueba un libro y redirige al panel de subidas.
	 *
	 * @param idLibro Id del libro.
	 * @return Redirección al panel de subidas.
	 */
	public String aprobarLibro(@PathVariable("id") Long idLibro) {
		logger.info("Intentado aprobar libro {}", idLibro);
		var book = bookRepo.findById(idLibro).orElseThrow(() -> {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Libro no encontrado.");
		});
		book.setAprobado(EstadoLibro.APROBADO);
		bookRepo.save(book);
		logger.info("Libro aprobado {}", idLibro);
		return "redirect:/admin/subidas?aprobar";
	}

	@PostMapping("subidas/denegar/{id}")
	/**
	 * Deniega un libro y redirige al panel de subidas.
	 *
	 * @param idLibro Id del libro.
	 * @return Redirección al panel de subidas.
	 */
	public String denegarLibro(@PathVariable("id") Long idLibro) {
		logger.info("Intentado denegir libro {}", idLibro);
		var book = bookRepo.findById(idLibro).orElseThrow(() -> {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Libro no encontrado.");
		});
		book.setAprobado(EstadoLibro.DENEGADO);
		bookRepo.save(book);
		logger.info("Libro denegado {}", idLibro);
		return "redirect:/admin/subidas?denegar";
	}
}

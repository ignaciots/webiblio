package org.ignaciots.webiblio.controllers;

import org.ignaciots.webiblio.model.EstadoLectura;
import org.ignaciots.webiblio.model.PuntuacionLibro;
import org.ignaciots.webiblio.services.LibroService;
import org.ignaciots.webiblio.services.RatingService;
import org.ignaciots.webiblio.services.UsuarioService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.server.ResponseStatusException;

@Controller
@RequestMapping("/ratings")
/**
 * Controlador relativo a los ratings.
 *
 * @author Ignacio Torre Suárez.
 *
 */
public class RatingsController {

	@Autowired
	private LibroService bookService;

	@Autowired
	private UsuarioService userService;

	@Autowired
	private RatingService ratingService;

	private Logger logger = LoggerFactory.getLogger(RatingsController.class);

	@PostMapping("fav/{id}")
	/**
	 * Añade o quita un libro de favoritos.
	 *
	 * @param idLibro Id del libro.
	 * @return Redirección a los detalles del libro.
	 */
	public String favorito(@PathVariable("id") Long idLibro) {
		logger.info("Intentando cambiar favorito a {}", idLibro);
		var usuario = userService.getUsuarioActual();
		var libro = bookService.getLibroAprobado(idLibro)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "El libro buscado no existe."));
		ratingService.toggleFavorito(libro, usuario.get());
		return "redirect:/libros/" + idLibro;
	}

	@GetMapping(value = "puntuacion/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	/**
	 * Actualiza la puntuación de un libro.
	 *
	 * @param idLibro    Id del libro.
	 * @param puntuacion Puntuación del libro.
	 * @return true
	 */
	public boolean actualizarPuntuacion(@PathVariable("id") Long idLibro,
			@RequestParam(name = "puntuacion", required = true) PuntuacionLibro puntuacion) {
		logger.info("Intentando puntuar {} con {}", idLibro, puntuacion);
		var usuario = userService.getUsuarioActual();
		var libro = bookService.getLibroAprobado(idLibro)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "El libro buscado no existe."));
		ratingService.guardarPuntuacion(libro, usuario.get(), puntuacion);
		return true;
	}

	@GetMapping(value = "estado/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	/**
	 * Actualiza el estado de un libro.
	 *
	 * @param idLibro       Id del libro.
	 * @param estadoLectura Estado del libro.
	 * @return true
	 */
	public boolean actualizarEstado(@PathVariable("id") Long idLibro,
			@RequestParam(name = "estadoLectura", required = true) EstadoLectura estadoLectura) {
		logger.info("Intentando cambiar estado de {} a {}", idLibro, estadoLectura);
		var usuario = userService.getUsuarioActual();
		var libro = bookService.getLibroAprobado(idLibro)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "El libro buscado no existe."));
		ratingService.guardarEstadoLectura(libro, usuario.get(), estadoLectura);
		return true;
	}
}

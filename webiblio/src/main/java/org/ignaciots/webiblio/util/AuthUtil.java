package org.ignaciots.webiblio.util;

import org.ignaciots.webiblio.model.NivelPrivacidad;
import org.ignaciots.webiblio.security.WebiblioUser;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

/**
 * Utilidades relativas a la autenticación.
 *
 * @author Ignacio Torre Suárez.
 *
 */
public class AuthUtil {
	public static boolean estaUsuarioAutenticado() {
		var usuarioActual = getUsuarioActual();

		return !(usuarioActual instanceof String);
	}

	public static boolean esUsuarioActualmenteAutenticado(String nombreUsuario) {
		var usuarioActual = getUsuarioActual();
		if (usuarioActual instanceof String) {
			return false;
		}
		return ((User) usuarioActual).getUsername().equals(nombreUsuario);
	}

	public static Object getUsuarioActual() {
		return SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}

	public static Long getIdUsuarioActual() {
		var usuarioActual = getUsuarioActual();
		if (usuarioActual instanceof String) {
			return null;
		}
		return ((WebiblioUser) usuarioActual).getId();
	}

	public static boolean puedeAccederPerfil(String nombreUsuario, NivelPrivacidad nivelPrivacidad) {
		if (nivelPrivacidad == NivelPrivacidad.PUBLICO) {
			return true;
		}
		return esUsuarioActualmenteAutenticado(nombreUsuario);
	}

	public static boolean esMismoUsuario(String nombreUsuario) {
		var usuarioActual = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (usuarioActual instanceof String) {
			return false;
		}
		return ((User) usuarioActual).getUsername().equals(nombreUsuario);
	}

}

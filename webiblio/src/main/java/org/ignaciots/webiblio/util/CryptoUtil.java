package org.ignaciots.webiblio.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * Utilidades relativas a la criptografía.
 *
 * @author Ignacio Torre Suárez.
 *
 */
public class CryptoUtil {

	private static final int BCRYPT_ROUNDS = 11;

	/**
	 * Obtiene un {@link BCryptPasswordEncoder} con un factor de coste adecuado.
	 *
	 * @return El {@link BCryptPasswordEncoder} generado.
	 */
	public static final BCryptPasswordEncoder getBCryptPasswordEncoder() {
		return new BCryptPasswordEncoder(BCRYPT_ROUNDS);
	}
}

-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 03, 2021 at 07:34 PM
-- Server version: 8.0.25-0ubuntu0.20.04.1
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `webiblio`
--

-- --------------------------------------------------------

--
-- Table structure for table `categoria`
--

CREATE TABLE `categoria` (
  `id` bigint NOT NULL,
  `nombre` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `categoria`
--

INSERT INTO `categoria` (`id`, `nombre`) VALUES
(4, 'Clásico'),
(5, 'Español'),
(3, 'Histórico'),
(6, 'Inglés'),
(7, 'Misterio'),
(1, 'Novela'),
(2, 'Terror');

-- --------------------------------------------------------

--
-- Table structure for table `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(35);

-- --------------------------------------------------------

--
-- Table structure for table `libro`
--

CREATE TABLE `libro` (
  `id` bigint NOT NULL,
  `anyo_publicacion` int NOT NULL,
  `aprobado` int NOT NULL,
  `autores` varchar(255) NOT NULL,
  `numero_paginas` int NOT NULL,
  `path_imagen` varchar(255) NOT NULL,
  `path_libro` varchar(255) NOT NULL,
  `sinopsis` varchar(1000) NOT NULL,
  `titulo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `libro`
--

INSERT INTO `libro` (`id`, `anyo_publicacion`, `aprobado`, `autores`, `numero_paginas`, `path_imagen`, `path_libro`, `sinopsis`, `titulo`) VALUES
(1, 1885, 1, 'Leopoldo Alas', 248, '/resources/img/libros/regenta.jpg', '/resources/libros/regenta.epub', 'Con La Regenta, la literatura española deja su huella en uno de los campos más relevantes de la literatura decimonónica, el que tiene como personaje central a la casada infiel. Esta novela nos ofrece un retrato minucioso de la sociedad más conservadora de la España del siglo XIX. Su mirada crítica y profundamente moralista se conjuga con su extraordinaria capacidad de análisis psicológico. Los personajes centrales, complejos y atormentados, quedan para siempre en nuestra memoria.» Soledad Puértolas En Vetusta como escenario único, Ana Ozores, infelizmente casada con el regente Víctor Quintanar, se debate entre el donjuán provinciano Álvaro Mesía y su confesor, el magistral don Fermín de Pas. A partir de este triángulo, Clarín recrea magistralmente la vida cotidiana de una ciudad de provincias del siglo XIX, el adulterio, el caciquismo, la ambición personal, el enfrentamiento entre el poder clerical y el secular…', 'La Regenta'),
(2, 1554, 1, 'Anónimo', 240, '/resources/img/libros/lazarillo.jpg', '/resources/libros/lazarillo.epub', 'El lazarillo de Tormes, novela de autor anónimo publicada en 1554, abrió un nuevo horizonte en la literatura española: se la considera precursora del género picaresco. Se trata de una novela escrita en primera persona como si se tratara de una carta larga, en la que Lázaro, el protagonista, un chico de origen muy humilde, nos cuenta sus desventuras y peripecias de amo en amo. Un relato lleno de ironía y un retrato despiadado de la sociedad de la época.', 'Vida de Lazarillo de Tormes y de sus fortunas y adversidades'),
(3, 1605, 1, 'Miguel de Cervantes Saavedra', 992, '/resources/img/libros/quijote.jpg', '/resources/libros/quijote.epub', 'El Quijote es considerada por muchos la primera novela moderna y se cuenta entre las principales obras de la literatura universal. Explica la historia de Alonso Quijano, un hidalgo que se vuelve loco al leer demasiados libros de caballerías. Así, decide cambiar su nombre por el de don Quijote de la Mancha, abandona su aldea y sale al mundo dispuesto a reparar daños u ofensas, junto con su fiel y humilde escudero, Sancho Panza. Pero los disparates y los malentendidos acompañarán a estos dos personajes en todas sus aventuras…', 'Don Quijote de la Mancha'),
(4, 1886, 1, 'Robert Louis Stevenson', 160, '/resources/img/libros/jekyllhyde.jpg', '/resources/libros/jekyllhyde.epub', 'Una novela que aborda un problema atemporal: la identidad del ser humano, la dualidad que todos llevamos en nuestro interior\r\nTodas estas preguntas se las planteó también Robert L. Stevenson cuando decidió escribir esta novela, que aborda un problema atemporal: la identidad del ser humano, la dualidad que todos llevamos en nuestro interior; en suma, el bien y el mal, tema tan viejo como la humanidad misma.', 'El caso extraño del Doctor Jekyll'),
(5, 1846, 1, 'Edgar Allan Poe', 123, '/resources/img/libros/cuentos_poe_1.jpg', '/resources/libros/cuentos_poe_1.epub', 'Recopilación de cuentos de Edgar Allan Poe: El Barril de Amontillado -- El Escarabajo de Oro -- La Ruina de la Casa de Úsher -- Ligeia -- La Máscara de la Muerte Roja -- El Crimen de la Rue Morgue -- El Gato Negro -- Un Descenso por el Maelström', 'Cuentos clásicos del norte, parte 1'),
(6, 1891, 1, 'Arthur Conan Doyle', 483, '/resources/img/libros/guardiablanca.jpg', '/resources/libros/guardiablanca.epub', 'Arthur Conan Doyle reproduce en La Guardia Blanca una serie de episodios  de aquella época tan agitada como fue para Inglaterra la segunda mitad del siglo XIV, en la que a pesar de sus recientes victorias de Crécy y Poitiers y del tratado de Bretigny, volvía a encenderse, más fiera y sañuda si cabe, aquella lucha interminable conocida en la historia con el nombre de Guerra de los Cien Años.', 'La guardia blanca'),
(7, 1844, 1, 'José Zorrilla', 160, '/resources/img/libros/donjuan.jpg', '/resources/libros/donjuan.epub', 'El arquetipo de Don Juan, entroncado con la tradición literaria del Siglo de oro español, se convirtió durante el siglo xix en uno de los personajes favoritos del romanticismo europeo. El drama de JOSÉ ZORRILLA (1817-1893), estrenado en 1844, incorporó definitivamente la figura de DON JUAN TENORIO al acervo de la cultura popular.', 'Don Juan Tenorio'),
(8, 1603, 1, 'William Shakespeare', 320, '/resources/img/libros/hamlet.jpg', '/resources/libros/hamlet.epub', 'Hamlet, probablemente compuesta entre 1599 y 1601, transcurre en Dinamarca y relata cómo el príncipe Hamlet lleva a cabo su venganza sobre su tío Claudio quien asesinase al padre de Hamlet, el rey, y ostenta la corona usurpada así como nupcias con Gertrudis, la madre de Hamlet. La obra se traza vívidamente alrededor de la locura (tanto real como fingida) y el transcurso del profundo dolor a la desmesurada ira. Además explora los temas de la traición, la venganza, el incesto y la corrupción moral.', 'Hamlet: drama en cinco actos'),
(9, 1502, 1, 'Fernando de Rojas', 256, '/resources/img/libros/celestina.jpg', '/resources/libros/celestina.epub', 'Que aun la misma vida de los hombres, si bien lo miramos, desde la primera edad hasta que blanquean las canas, es batalla , afirma Fernando de Rojas, autor de La Celestina, y simboliza en esa frase la tensión dramática que recorre todo el libro.Esta obra -la única conocida del autor- es considerada precursora de la novela picaresca, adelantándose a El Quijote. Describe el amor apasionado de dos jóvenes, Calisto y Melibea, y la intervención de una vieja astuta, Celestina, la casamentera. El nombre de este personaje ha pasado a formar parte del lenguaje diario, como símbolo de la encubridora de amores clandestinos. En la obra se tratan tres grandes temas medievales: el amor, la fortuna y la muerte.', 'La Celestina'),
(10, 1873, 1, 'Benito Pérez Galdós', 149, '/resources/img/libros/trafalgar.jpg', '/resources/libros/trafalgar.epub', 'Trafalgar (1873) es el primero de los Episodios nacionales de Galdós, serie que constituye la más vasta construcción novelesca que registra la historia de nuestras letras. Los hechos bélicos que trata Galdós tuvieron lugar en 1805. Para su elaboración utilizó textos históricos, pero también fuentes orales.', 'Trafalgar'),
(11, 1891, 1, 'Leopoldo Alas', 189, '/resources/img/libros/unicohijo.jpg', '/resources/libros/unicohijo.epub', 'Oscurecida muchos años por el éxito de La Regenta, esta otra novela, Su único hijo, que Leopoldo Alas, Clarín, publica en 1891, ha ido obteniendo en los últimos tiempos un reconocimiento cada vez más positivo que la ha aproximado en estimación a aquella obra universal. Su tema central -la insegura paternidad de un niño- sirve de trama al autor para tejer un magistral discurso novelístico sobre la ambigüedad de todo cuanto existe. Superando la posición fija del narrador tradicional, y en la línea de la mejor novela europea de la época, Clarín cambia aquí constantemente el punto de vista y proyecta sobre la vida una mirada irónica que, al tiempo que la ilumina, la hace problemática.', 'Su único hijo'),
(12, 1916, 1, 'Leopoldo Alas', 338, '/resources/img/libros/doctorsutilis.jpg', '/resources/libros/doctorsutilis.epub', 'Una obra que recoge 28 hermosos cuentos de Leopoldo Alas, Clarín.', 'Doctor Sutilis (Cuentos)'),
(13, 1871, 1, 'Ignacio Manuel Altamirano', 182, '/resources/img/libros/navidadmontanas.jpg', '/resources/libros/navidadmontanas.epub', 'En este libro el autor descubre el verdadero valor de la Navidad y narra las dificultades que prevaleeíau en ese tiempo entre católicos y liberales. Además describe los hábitos del pueblo donde se escenlilca esta novela, y mediante twa pareja que se separa, nos da su opinión acerca de los valores humanos, como la generosidad y la ternura, que existían en esa época. También eombina la frivolidad de una historia romántica desarrollada en lugares y situaciones idílicas, y con un extraordinario lenguaje, lleno de ternura, logra que La Navidad en las montañas se convierta en una novela de gran valor.', 'La Navidad en las Montañas'),
(14, 1899, 1, 'José S. Alvarez', 88, '/resources/img/libros/memoriasvigilante.jpg', '/resources/libros/memoriasvigilante.epub', 'Memorias de un vigilante es una obra diferente. Ambientada en la Argentina del siglo XIX nos presenta, con un detalle que goza en ocasiones de una profundidad excesivamente detallada para una obra de tales características, la vida de un funcionario estatal, un vigilante. El día a día de sus obligaciones como policía, aderezadas con un vaivén de recuerdos y situaciones que van desde la simple anécdota a la más singular aventura, nos sirven de marco para mostrarnos una sociedad que, lejos de lo que pudiera parecer, no se diferencia en mucho de la actual.', 'Memorias de un vigilante'),
(15, 1919, 1, 'Leonid Andreyev', 198, '/resources/img/libros/losespectros.jpg', '/resources/libros/losespectros.epub', 'Cuando ya no cupo duda de que Egor Timofeievich Pomerantzev, el subjefe de la oficina de Administración local, había perdido definitivamente la razón, se hizo en su favor una colecta, que produjo una suma bastante importante, y se le recluyó en una clínica psiquiátrica privada.\r\nAunque no tenía aún derecho al retiro, se le concedió, en atención a sus veinticinco años de servicios irreprochables y a su enfermedad. Gracias a esto, tenía con que pagar su estancia en la clínica hasta su muerte: no había la menor esperanza de curarle.', 'Los espectros: Novelas breves'),
(16, 1890, 1, 'Arthur Conan Doyle', 160, '/resources/img/libros/signfour.jpg', '/resources/libros/signfour.epub', 'Hay mitos culturales imperecederos, que ya por siempre acompañarán los pasos de la humanidad sobre este viejo planeta. Uno de ellos es el de Sherlock Holmes. Pero, si no han de conocer nunca un fin, estos mitos tuvieron un principio. Es un Holmes joven -en esta obra aparecía por segunda vez de la mano de su creador, A. Conan Doyle -, y sus recursos, el que en El signo de los cuatro se enfrenta al mal, entrevenado aquí de sutiles elementos orientales que subrayan con su exotismo el clima de misterio, en las brumosas orillas del Támesis. El lector juzgará el grado de éxito alcanzado por el sagaz detective en esta narración, donde se equilibran de forma magistral la introspección deductiva y la acción.', 'The Sign of the Four'),
(17, 1902, 1, 'Arthur Conan Doyle', 256, '/resources/img/libros/houndbaskervilles.jpg', '/resources/libros/houndbaskervilles.epub', 'La más famosa de las historias de Sherlock Holmes, El sabueso de los Baskerville presenta el fantasma del sabueso de Dartmoor, el que, según cuenta la leyenda, ha acosado a generaciones de Baskervilles. Cuando Sir Charles Baskeville muere repentinamente de un ataque cardíaco en la finca de su familia, los lugareños se hallan convencido que el fantasma del famoso sabueso es el responsable. Sherlock Holmes es llamado una vez más- para resolver el extraño misterio.', 'The Hound of the Baskervilles'),
(18, 1888, 1, 'Arthur Conan Doyle', 288, '/resources/img/libros/studyscarlet.jpg', '/resources/libros/studyscarlet.epub', 'Es ésta la novela en que Conan Doyle dio a conocer al inmortal detective Sherlock Holmes, y al doctor Watson, su no menos genial narrador. Un cadáver hallado en extrañas circunstancias pone en marcha los reflejos deductivos de Holmes, mientras la policía oficial se pierde en divagaciones equivocadas o arresta a inocentes ciudadanos. Un nuevo asesinato parece complicar la historia, pero a Holmes se la aclara. Nuestro detective no sólo encuentra al asesino, sino que intuye la historia turbulenta que lo motiva: la de otros asesinatos ocurridos treinta años atrás y cuyos ecos llegan al presente, historia que constituye una segunda novela tan apasionante como la primera.', 'A Study in Scarlet'),
(19, 1920, 1, 'Agatha Christie', 296, '/resources/img/libros/affairstyles.jpg', '/resources/libros/affairstyles.epub', 'Essex, Inglaterra. La millonaria Emily Inglethorp amanece muerta en su habitación sin indicio alguno de violencia. Aunque la policía descarta que se trate de un asesinato, demasiadas rivalidades en la vieja mansión propiedad de la fallecida hacen pensar en un posible caso de envenenamiento que podría haber pasado desapercibido. Cuando el detective Hércules Poirot llega para encargarse de la investigación, se encuentra frente a frente con la avaricia, los celos, las tensiones y la ambición de una familia que aspira a heredar una fortuna en dinero y propiedades. Un marido infiel, su jovencísima amante, unos hijastros envidiosos, un extraño toxicólogo alemán… Todos parecen sospechosos de haber acabado con la vida de Emily, aunque solo uno de ellos puede ser el asesino. Poirot deberá emplearse a fondo y usar todas sus armas para llegar al fondo de su primer caso literario.', 'The Mysterious Affair at Styles'),
(20, 1922, 1, 'Agatha Christie', 320, '/resources/img/libros/secretadversary.jpg', '/resources/libros/secretadversary.epub', 'Autoproponerse como «jóvenes aventureros dispuestos a hacer lo que sea» demuestra ser una jugada inteligente por parte de Tommy y Tuppence Beresford. El primer trabajo parece un sueño. Todo lo que Tuppence debe hacer es emprender un viaje con gastos pagados a París y hacerse pasar por una americana llamada Jane Finn. Pero el trabajo se convierte en una amenaza silenciosa y en un riesgo para sus vidas tras la desaparición de la persona que los ha contratado. Ahora, el nuevo trabajo de Tuppence será el de detective, ya que, si en algún lado hay una «Jane Finn» que existe realmente, esta posee un secreto que pone la vida de ambas en peligro…', 'The Secret Adversary'),
(21, 1911, 1, 'Edgar Allan Poe', 180, '/resources/img/libros/poemaspoe.jpg', '/resources/libros/poemaspoe.epub', 'Recopilación de poemas de Edgar Allan Poe.', 'Poemas');

-- --------------------------------------------------------

--
-- Table structure for table `libro_categoria`
--

CREATE TABLE `libro_categoria` (
  `id_libro` bigint NOT NULL,
  `id_categoria` bigint NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `libro_categoria`
--

INSERT INTO `libro_categoria` (`id_libro`, `id_categoria`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(13, 1),
(14, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(5, 2),
(21, 2),
(6, 3),
(10, 3),
(1, 4),
(2, 4),
(3, 4),
(4, 4),
(5, 4),
(7, 4),
(8, 4),
(9, 4),
(10, 4),
(11, 4),
(16, 4),
(17, 4),
(18, 4),
(19, 4),
(20, 4),
(21, 4),
(1, 5),
(2, 5),
(3, 5),
(7, 5),
(9, 5),
(10, 5),
(11, 5),
(12, 5),
(13, 5),
(14, 5),
(15, 5),
(21, 5),
(16, 6),
(17, 6),
(18, 6),
(19, 6),
(20, 6),
(16, 7),
(17, 7),
(18, 7),
(19, 7),
(20, 7);

-- --------------------------------------------------------

--
-- Table structure for table `lista`
--

CREATE TABLE `lista` (
  `id` bigint NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `id_usuario` bigint NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `lista`
--

INSERT INTO `lista` (`id`, `nombre`, `id_usuario`) VALUES
(30, 'Lista 1', 1),
(31, 'Lista 2', 1),
(32, 'Lista 3', 1);

-- --------------------------------------------------------

--
-- Table structure for table `lista_libro`
--

CREATE TABLE `lista_libro` (
  `id_lista` bigint NOT NULL,
  `id_libro` bigint NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `lista_libro`
--

INSERT INTO `lista_libro` (`id_lista`, `id_libro`) VALUES
(30, 16),
(30, 17),
(30, 18),
(31, 18),
(31, 19),
(31, 20);

-- --------------------------------------------------------

--
-- Table structure for table `rating_usuario_libro`
--

CREATE TABLE `rating_usuario_libro` (
  `id_libro` bigint NOT NULL,
  `id_usuario` bigint NOT NULL,
  `estado` varchar(255) DEFAULT NULL,
  `favorito` bit(1) DEFAULT NULL,
  `puntuacion` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `rating_usuario_libro`
--

INSERT INTO `rating_usuario_libro` (`id_libro`, `id_usuario`, `estado`, `favorito`, `puntuacion`) VALUES
(4, 1, NULL, b'1', NULL),
(16, 1, 'LEIDO', b'0', 3),
(17, 1, 'LEIDO', b'1', 4),
(18, 1, NULL, b'0', 3),
(19, 1, 'LEIDO', b'1', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

CREATE TABLE `usuario` (
  `id` bigint NOT NULL,
  `estado` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `nivel_privacidad` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `nombre_usuario` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `password` varchar(512) COLLATE utf8mb4_bin NOT NULL,
  `rol` varchar(255) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `usuario`
--

INSERT INTO `usuario` (`id`, `estado`, `nivel_privacidad`, `nombre_usuario`, `password`, `rol`) VALUES
(1, 'HABILITADO', 'PUBLICO', 'user', '$2a$11$gE12WrHFVsjJ4wKLUGNrfucs2R2DD/MwqQaJh/G5ru/DiNK78hjJm', 'USUARIO'),
(2, 'HABILITADO', 'PRIVADO', 'mod', '$2a$11$ckOJzSiasuR8/L9fu0Spxu7pAH49obCKwpsOKNYTWe1liKNCPda.S', 'MODERADOR'),
(3, 'HABILITADO', 'PRIVADO', 'admin', '$2a$11$oGOTU3Y/0ucHUPPBxjkWkexIEaocfaBsZVkiUUlHmJYNWFekivXwu', 'ADMINISTRADOR');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_35t4wyxqrevf09uwx9e9p6o75` (`nombre`);

--
-- Indexes for table `libro`
--
ALTER TABLE `libro`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `libro_categoria`
--
ALTER TABLE `libro_categoria`
  ADD PRIMARY KEY (`id_libro`,`id_categoria`),
  ADD KEY `FK8e0bwu48ll8t38s29sywah57o` (`id_categoria`);

--
-- Indexes for table `lista`
--
ALTER TABLE `lista`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKnwhmp66ry2ju6xpvh977svha3` (`id_usuario`);

--
-- Indexes for table `lista_libro`
--
ALTER TABLE `lista_libro`
  ADD PRIMARY KEY (`id_lista`,`id_libro`),
  ADD KEY `FKmmv0medoqg7ghivps8gflb1jk` (`id_libro`);

--
-- Indexes for table `rating_usuario_libro`
--
ALTER TABLE `rating_usuario_libro`
  ADD PRIMARY KEY (`id_libro`,`id_usuario`),
  ADD KEY `FKxs64ssdfyvlln1aka7ybmgg4` (`id_usuario`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_puhr3k3l7bj71hb7hk7ktpxn0` (`nombre_usuario`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `libro_categoria`
--
ALTER TABLE `libro_categoria`
  ADD CONSTRAINT `FK8e0bwu48ll8t38s29sywah57o` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id`),
  ADD CONSTRAINT `FKm84a6si5xhxgjswpxuvikubw4` FOREIGN KEY (`id_libro`) REFERENCES `libro` (`id`);

--
-- Constraints for table `lista`
--
ALTER TABLE `lista`
  ADD CONSTRAINT `FKnwhmp66ry2ju6xpvh977svha3` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id`);

--
-- Constraints for table `lista_libro`
--
ALTER TABLE `lista_libro`
  ADD CONSTRAINT `FKfa7bvonl8hlvi98yfeknom0em` FOREIGN KEY (`id_lista`) REFERENCES `lista` (`id`),
  ADD CONSTRAINT `FKmmv0medoqg7ghivps8gflb1jk` FOREIGN KEY (`id_libro`) REFERENCES `libro` (`id`);

--
-- Constraints for table `rating_usuario_libro`
--
ALTER TABLE `rating_usuario_libro`
  ADD CONSTRAINT `FKr2311x5y35xwg522ylkv3u4p8` FOREIGN KEY (`id_libro`) REFERENCES `libro` (`id`),
  ADD CONSTRAINT `FKxs64ssdfyvlln1aka7ybmgg4` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
